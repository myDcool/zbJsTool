# JS-Tool

#### 介绍
常用的js组件合集, 每个js组件都可以单独使用, js源码**文件的尾部有使用样例**

#### 插件列表
##### JS
> js中都自带css (/es6/*.js)
- zbRepeat.js 根据 数组/对象, html模板 渲染出html代码
- zbASArea.js 省市县插件, 继承了数据, 可模糊搜索
- zbASCheckbox.js 多选插件
- zbASRadio.js 单选插件
- zbASText.js input/textarea 插件, 支持字符长度限制和已输入数量显示
- zbEditTable.js 简单的可编辑表格, 支持批量改数据
- zbFetch.js 网络请求的简单封装, 有post,get两个方法
- zbImageMerge.js 图片合并, 支持图片缩放, 横向和竖向合并
- zbImageQr.js 支持在二维码下方插入文字(有依赖)
- zbList.js 生成列表, 支持点击回调(页面中会有箭头图标), 和没回调两种显示样式
- zbPanel.js 可折叠列表
- zbRouter.js 单页面路由(根据 `?m=xxx&a=1&b=2`进行路由) 不刷新页面, 只更改URL
- zbScroll.js 页面滚动, 比如滚动到顶部
- zbSelectImg.js 选择一个图片并回显的页面中, 支持浮层显示
- zbTabNav.js 顶部导航插件
- zbToolBar.js 工具栏插件, 支持显示在顶部或底部
- zbUploadFiles.js 文件上传插件, 支持上传一个(upOne), 或多个(upLot)
- zbASComplete.js 自动补全

##### CSS   
- flex.css flex样式排列组合, 减少代码量

### 使用说明
- 参考各个js文件底部的使用说明

