import zbTool from "./zbTool.js"
import zbAsArea from "./zbAsArea.js"
import zbAsCheckbox from "./zbAsCheckbox.js"
import zbAsRadio from "./zbAsRadio.js"
import zbAsText from "./zbAsText.js"
import zbList from "./zbList.js"
import zbPanel from "./zbPanel.js"
import zbRepeat from "./zbRepeat.js"
import zbFetch from "./zbFetch.js"
import zbRouter from "./zbRouter.js"
import zbSelectImg from "./zbImageSelect.js"
import zbTabNav from "./zbTabNav.js"
import zbToolBar from "./zbToolBar.js"
import zbUploadImage from "./zbUploadFiles.js"
import zbAsComplete from "./zbAsComplete.js"


// let tool = new zbTool;
// console.log(tool.getDay());
///////////////////// 
// let area = new zbAsArea({
// 	title: '请输入地址',
// 	params: {a:1, b:2}, //透传参数
// 	onSelected: function(data, params){
// 		console.log(data);
// 		console.log(params);
// 	}
// });
// area.show();
/////////////////////
// let checkbox = new zbAsCheckbox({
// 	title: '标题',
// 	data: [
//         {title:'a', value:1, img:'/icons/right-arrow.png'},
//         {title:'b', value:2, img:'/icons/right-arrow.png'},
//         {title:'c', value:3, img:'/icons/right-arrow.png'},
//         {title:'d', value:4, img:'/icons/right-arrow.png'},
//         {title:'e', value:5}
//     ],
// 	params: {a:1, b:2}, //透传参数
// 	item_class: 'item_left', //可选 item_center,item_left
// 	item_img_class: 'item_img',
// 	onSelected: function(params) {
// 	    console.log(params);
// 	},
// 	onConfirm: function(data, params) {
//         console.log(data); //被点击的数据
//         console.log(params); //透传参数
//     }

// });
// checkbox.show();

// let checkbox = new zbAsCheckbox({
// 	title: '标题',
// 	data: [1,2,3,4,5,6],
//  	params: {a:1, b:2}, //透传参数
// 	item_class: 'item_left', //可选 item_center,item_left
// 	item_img_class: 'item_img',
// 	onSelected: function(params) {
// 	    console.log(params);
// 	},
// 	onConfirm: function(data, params) {
//     console.log(data); //被点击的数据
//     console.log(params); //透传参数
//     }

// });
// checkbox.show();
///////////////////
// let radio = new zbAsRadio({
// 	title: '单选',
// 	data: [
//         {title:'a', value:1, img:'summer/icons/right-arrow.png'},
//         {title:'b', value:2, img:'summer/icons/right-arrow.png'},
//         {title:'c', value:3, img:'summer/icons/right-arrow.png'},
//         {title:'d', value:4, img:'summer/icons/right-arrow.png'},
//         {title:'e', value:5}
//     ],
//     item_class: 'item_left', //可选 item_center,item_left
// 	item_img_class: 'item_img',
// 	params: {a:1, b:2}, //透传参数
//     onSelected: function(data, params) {
//         console.log(data); //被点击的数据
//         console.log(params); //透传参数
//     },
//     onShow: function(){},
//     onHide: function(){},

// });
// radio.show();
//////////////////
// let input = new zbAsText({
// 	title: '请输入文本',
// 	params: {a:1, b:2}, //透传参数
// 	// config: {id:'test',  type:'number', value: 0, maxlength: 5, step: 1,autocomplete:'off', autofocus:true},
//     // config: {id:'test',  type:'number', 		value: 0, maxlength: 5, step: 0.1,autocomplete:'off',},
//     // config: {id:'test',  type:'text', 		value: 'aaa', maxlength: 10, autocomplete:'off',},
//     // config: {id:'test',  type:'date', 		value: '2023-01-01', maxlength: 10, autocomplete:'off',},
//     // config: {id:'test',  type:'datetime-local', value: '2023-01-01T00:00:00', min: '1900-01-01T00:00:00', step:1,  autocomplete:'off',},
//     // config: {id:'test',  type:'email', 		value: '', maxlength:100,  autocomplete:'off',},
//     config: {id:'test',  type:'textarea', 	value: '1111', maxlength:300,  autocomplete:'off',},

// 	onConfirm: function (data, params) {
// 		console.log(data);
// 		console.log(params);
// 	},
// 	onShow: function(){},
//     onHide: function(){},
// });
// input.show();
///////////////////
// let mylist = new zbList({
// 	list_type: 'click', //list_type: click|text
// 	list_id: 'target',
// 	data: [
// 		{
// 		       item_id: '1111',
// 		       title:'测试-name1',
// 		       desc: 'Hello World!',
// 		       params: 'xxx',
// 		   },
// 		{
// 		       item_id: '2222',
// 		       title:'测试-name2',
// 		       desc: 'Hello World!',
// 		       params: 'xxx',
// 		   },
// 		],
// 	onClick: function(item_data, clicked){
// 	    console.log(item_data); //列表项元素
// 	    console.log(clicked); //被直接点击的元素
// 	}
// });
// mylist.replaceNode('target'); ////将id=content的节点替换为list



// let children = [
// {
//        item_id: '3333',
//        title:'测试-name3',
//        desc: 'Hello World!',
//        params: 'xxx',
//    },
// {
//        item_id: '4444',
//        title:'测试-name4',
//        desc: '<p>Hello World!</p>',
//        params: 'xxx',
//    },
// ];
// mylist.append(children);
//////////////////////////
// let panel = new zbPanel({
// 	panel_id: 'target',
// 	data: [
// 	    {
// 	       'panel_id': '1111',
// 	       'title':{
// 	           'idx':1, //可以不写
// 	           'name':'测试-name1',
// 	       },
// 	       'body':{ 'content': '<p>Hello World!</p>' },
// 	       'footer':{ 'content':'1111111' }
// 	   },
// 	    {
// 	       'panel_id': '222',
// 	       'title':{
// 	           'idx':2,
// 	           'name':'测试-name2',
// 	       },
// 	       'body':{ 'content': '<p>Hello World!</p>' },
// 	       'footer':{ 'content': '222222' }
// 	   }
// 	],
// 	onHide: function(id){},
// 	onShow: function(id){panel.updateBodyContent(id, '内容更新啦~' + id);}
// });

// panel.replaceNode('target'); //将id为target的元素替换成本插件生成的node
////////////////////////
// let repeat = new zbRepeat();
// let json_list = [{name:'张三', age:18, fav:1},{name:'李四', age:18, fav:2},{name:'王五', age:18, fav:3},{name:'赵六', age:18, fav:4}];

// //循环innerHTML
// repeat.repeatInnerHTML('#tpl_repeat_inner', json_list);
// repeat.repeatOuterHTML('.tpl_repeat_outer', json_list); 

///////////////////////
// let req = new zbFetch();
// req.post('http://www.test7.com/test.php', {a:1, b:2}, function(rs){console.log(rs);});
// req.get('http://www.test7.com/test.php', function(rs){console.log(rs);});
///////////////////////
//let routers = {
//    index: {tpl:'t1', target:'p1', callback: function(args){console.log(args);}},
//    list: {tpl:'t2', target:'p2',callback: function(args){console.log(args);}},
//    detail: {tpl:'t3', target:'p3',callback: function(args){console.log(args);}},
//};
//let r = new zbRouter(routers, 'index');
//r.hookBefore = function(rule, args){
//    //hook, 执行回调前执行
//    console.log(rule);
//    console.log(args);
//};
//
//r.route(r.decodeRule()); //根据当前url地址执行路由
///////////////////////
// document.getElementById('choseImage').addEventListener('click', function(e){
//     let si = new zbSelectImg({
//         onSelected: function(args) {console.log(args);},
//         onClickImg: function(e) {
//         	console.log(e.target);
//         	si.showMask(e.target);
//         },
//         params: {a:1, b:2}
//     })

//     si.choseImage({
//         attr:{}, 
//         img_box_id:'image_box', 
//         tile:'title'
//     })
// });
//////////////////////
// let navCate = new zbTabNav({
//    nav_id: 'navCate', //导航最外部的ID
//    content_id: 'content', //导航内容区域的ID
//    active_class:'zb-tabnav-active', //active_class:高亮当前选中tab的样式名, 可以不传
//    tabs: [{name:'1111', id:'1111'}, {name:'22222', id:'2222'}], //导航标题信息
//    onChangeTab: function (id) {
//        navCate.updateContent('html here ...' + id);
//        //navCate.updateNode(node here...);
//    }
   
// });

// navCate.replaceDom('target');//把HTML替换到指定id的dom
// navCate.changeTab(0); //触发导航切换, 参数为整数, 表示tab的索引
// // navCate.changeTabById('2222'); //触发导航切换, 参数为字符串, 表示tab的id属性
////////////////////////
// let tb = new zbToolBar({
// 	nav_id: 'tb',
// 	position:'bottom',
// 	items: [
// 	    {name: '111', id: '111', style:'bg-grey'},
// 	    {name: '222', id: '222', style:'default'},
// 	    {name: '333', id: '333', style:'bg-green'},
// 	],
// 	onClick: function (item) {
// 		console.log(item); //{name: '111', id: '111', style:'bg-grey'}
// 	}
// });

// tb.replaceNode("target"); //将id=aaa的标签替换为 toolbar
///////////////////////
// document.getElementById('file').addEventListener('input', function() {
// 	let imageUp = new zbUploadFiles({
// 		upload_url: 'http://www.test7.com/test.php',
// 		file_input_id: 'file',
// 		params: {a:1, b:2},
// 		uploadSuccess: function (rs) {
// 		    console.log(rs);
// 		},
// 		uploadFailed: function (error) {
// 		    console.log(error);
// 		}
// 	});
// 	imageUp.upOne();
// 	//imageUp.upLot();
// })
/////////////////////////
let cmp = new zbAsComplete({
    title: '自动补全',
    params: {a:1, b:2}, //透传参数
    onSelected: function(selected, params){
        console.log(selected); //选中的数据
        console.log(params); //透传参数
    },
    data: [{content:'aaa'},{content:'bbb'},{content:'ccc'}]
});
cmp.show();
