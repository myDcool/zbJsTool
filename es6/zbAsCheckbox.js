//多选列表
export default class zbAsCheckbox {
    constructor (config) {
        this.title = config.title ??= '多选';//弹窗标题
        this.data = config.data ??= [];//所有数据
        this.item_class = config.item_class ??= 'item_center';//所有数据
        this.item_img_class = config.item_img_class ??= 'item_img';//所有数据
        this.onSelected = config.onSelected ??= null;//点击列表后执行回调
        this.onConfirm = config.onConfirm ??= null;//点击确认按钮后的回调
        this.onShow = config.onShow ??= null;//显示弹窗前的回调
        this.onHide = config.onHide ??= null;//点击空白关闭弹窗时的回调
        this.isConfirm = 0; //是否已点击过确认按钮, 防止频繁点击
        this.params = config.params ??= {};//透传参数

        //浮层模板
        this.tpl = `<div id="zbAsCheckbox">
            <div id="zbCbMask"></div>
            <div id="zbCbBox">
              <div id="zbCbTitle"></div>
              <div id="zbCbBody" class="zb-scroll-y"></div>
              <div id="zbCbFooter">
               <div class="zbCbBtn" id="zbCbBtnCancel">取消</div>
               <div class="zbCbBtn" id="zbCbBtnConfirm">确认</div>
              </div>
             </div>
             <input type="hidden" id="zbCbInput" value="">
            </div>`;

        this.init();
    }

    init () {
        //初始化css
        this.initCss();

        //初始模板, 绑定事件
        let body = document.getElementsByTagName('body')[0];
        let node = this.htmlToNode(this.tpl);
        node.querySelector('#zbCbMask').addEventListener('click', this.hide.bind(this));
        node.querySelector('#zbCbBtnCancel').addEventListener('click', this.hide.bind(this));
        node.querySelector('#zbCbBtnConfirm').addEventListener('click', this.confirm.bind(this));
        body.appendChild(node);

        //标题
        document.getElementById('zbCbTitle').innerText = this.title;
        this.addList(this.data);
        return this;
    }

    htmlToNode (html) {
        let div = document.createElement('div');
        div.innerHTML = html;
        return div.firstElementChild;
    }

    //隐藏弹出层
    hide () {
        document.getElementById('zbAsCheckbox').remove(); //彻底移除了心静
        if (typeof this.onHide === 'function') {
            this.onHide();
        }
    }

    //显示弹出层
    show () {
        if (typeof this.onShow === 'function') {
            this.onShow();
        }
        document.getElementById('zbAsCheckbox').style.height = '100%';
    }

    //点击确认按钮触发执行
    confirm () {
        if (this.isConfirm === 1) {
            console.log('重复点击');
            return;
        }
        this.isConfirm = 1;
        let selected = document.getElementById('zbCbBody').getElementsByClassName('zb-response');
        let data = [];
        for (let i=0; i<selected.length; i++) {
            let idx = selected[i].getAttribute('idx');
            data.push(this.data[idx]);
        }
        this.hide();
        if (typeof this.onConfirm === 'function') {
            this.onConfirm(data, this.params);
        }
    }

    //生成样式
    initCss() {
        let id = 'zbAsCheckboxCss';
        if (document.getElementById(id)) {
            return;
        }
        let style = document.createElement('style');
        style.setAttribute('id', id);
        style.innerText =
            '.zb-scroll-x {overflow-x:scroll; white-space: nowrap;}'+
            '.zb-scroll-x::-webkit-scrollbar {display:none}'+
            '.zb-scroll-y {overflow-y:scroll; white-space: nowrap;}'+
            '.zb-scroll-y::-webkit-scrollbar {display:none}'+
            '.zb-scroll{overflow:scroll; white-space: nowrap;}'+
            '.zb-scroll::-webkit-scrollbar {display:none}'+

            '#zbAsCheckbox{position:fixed;top:0;height:0;left:0;right:0;z-index:200;overflow:hidden;background-color:rgba(0,0,0,0.4); display:flex;flex-direction:column;justify-content:space-between;}'+
            '#zbAsCheckbox #zbCbMask{height:100%}'+
            '#zbAsCheckbox #zbCbBox{max-height:90%;background-color:#fff;border-top-left-radius:6px;border-top-right-radius:6px;}'+
            '#zbAsCheckbox #zbCbTitle{height:40px;line-height:40px;font-size:20px;border-bottom:1px solid #eeeeee;text-align:center}'+
            '#zbAsCheckbox #zbCbBody {min-height:100px;max-height:500px; padding:3px;}'+
            '#zbAsCheckbox #zbCbBody .zbas-list-item {padding:3px;height:30px;border-bottom:1px solid #e8e8e8}'+
            '#zbAsCheckbox #zbCbBody .item_center{display:flex; flex-direction:row;justify-content: center; align-items: center;}'+
            '#zbAsCheckbox #zbCbBody .item_left{display:flex; flex-direction:row;justify-content: left; align-items: center;}'+
            '#zbAsCheckbox #zbCbBody .item_right{display:flex; flex-direction:row;justify-content: right; align-items: center;}'+
            '#zbAsCheckbox #zbCbBody .item_img{height:30px;width:30px;margin-right:5px;}'+
            '#zbAsCheckbox #zbCbFooter {padding:5px;border-top:1px solid #eeeeee;height:40px;line-height:40px;display:flex; flex-direction:row; justify-content:center}'+
            '#zbAsCheckbox .zbCbBtn{width:50%;font-size:20px;text-align:center;}'+
            '.zb-response{background-color:#eee}'+
            '.response {animation:bg_color 1s;}'+
            '@keyframes bg_color {from{background:#eee;} to{background:#fff;}}'+
            '@-webkit-keyframes bg_color {from{background:#eee;} to{background:#fff;}}'
        ;

        let head = document.getElementsByTagName('head')[0];
        head.appendChild(style);
    }

    //覆盖指定id的dom元素
    replaceNode (id, node){
        let old = document.getElementById(id);
        let parent = old.parentNode;
        parent.replaceChild(node, old);
    }

    /**
     * 添加纵向列表数据
     */
    addList() {
        let data = this.data;
        let item_class = this.item_class;
        let item_img_class = this.item_img_class;

        let tplListItem = '<div class="zbas-list-item {item_class}" idx="{idx}">{title}</div>';
        let tplListItemImg = '<div class="zbas-list-item {item_class}" idx="{idx}"><img class="{item_img_class}" src="{img}">{title}</div>';

        let dataBody = document.getElementById('zbCbBody');
        let strItems = '';
        for (let i=0; i<data.length; i++) {
            let item = data[i];
            if (this.isObject(item)) {
                // [{title:'aaa', value:'aaa', img:'', a:1, b2}]
                let tpl = (item['img'] !== undefined) ? tplListItemImg : tplListItem;
                strItems += this.repeatString(tpl, [data[i]], function(row){
                    row.idx = i;
                    row.item_class = item_class;
                    row.item_img_class = item_img_class;
                    return row;
                });
            } else {
                //[1,2,3]
                strItems += this.repeatString(tplListItem, [item], function(v){
                    let row = {title: v, value: v};
                    row.idx = i;
                    row.item_class = item_class;
                    return row;
                });
            }
        }

        //渲染列表
        dataBody.innerHTML = strItems;

        //组装每一列, 添加事件绑定
        let items = dataBody.getElementsByClassName('zbas-list-item');
        for (let i=0; i<items.length; i++) {
            items[i].addEventListener('click', this.click.bind(this));
        }

        return this;
    }

    //记录点击的list值
    click (e) {
        let obj = e.target;

        if (obj.classList.contains('zb-response')) {
            //删除已选择的项
            obj.classList.remove('zb-response');
        } else {
            //高亮当前选中
            obj.classList.add('zb-response');
        }

        if (typeof this.onSelected === 'function') {
            let idx = obj.getAttribute('idx');
            this.onSelected(this.data[idx]);
        }
    }

    //根据数组, 渲染HTML字符串
    repeatString (tplDom, arr, func=null) {
        if (!tplDom.length) {
            this.error('字符串长度为空');
            return;
        }

        if (arr.length === 0) {
            this.error('数据为空');
            return tplDom;
        }

        let tpl = tplDom;
        let out = '';
        for (let i=0; i<arr.length; i++) {
            if (typeof func === 'function') {
                arr[i] = func(arr[i]);
            }
            let map = arr[i];
            let tmp = tpl;
            for (let j in map) {
                let re = new RegExp('{' + j + '}', 'g');
                tmp = tmp.replace(re, map[j]);
            }

            let re = new RegExp('{_idx}', 'g');
            tmp = tmp.replace(re, parseInt(i)+1);

            out += tmp;
        }

        return out;
    };

    //是否是数组
    isArray (o){
        return Object.prototype.toString.call(o) === '[object Array]';
    }

    isObject (o) {
        return Object.prototype.toString.call(o) === '[object Object]';
    }

    error(str) {
        console.log(str);
    }
}
/**
 * 用法举例:
let checkbox = new zbAsCheckbox({
    title: '标题',
    data: [
        {title:'a', value:1, img:'/icons/right-arrow.png'},
        {title:'b', value:2, img:'/icons/right-arrow.png'},
        {title:'c', value:3, img:'/icons/right-arrow.png'},
        {title:'d', value:4, img:'/icons/right-arrow.png'},
        {title:'e', value:5}
    ],
    item_class: 'item_left', //可选 item_center,item_left,item_right 默认item_center
    item_img_class: 'item_img',
    onSelected: function(params) {
        console.log(params);
    },
    onConfirm: function(params) {
        console.log(params);
    }

});
checkbox.show();


let checkbox = new zbAsCheckbox({
    title: '标题',
    data: [1,2,3,4,5,6],
    item_class: 'item_left', //可选 item_center,item_left
    item_img_class: 'item_img',
    onSelected: function(params) {
        console.log(params);
    },
    onConfirm: function(params) {
        console.log(params);
    }

});
checkbox.show();

 */
