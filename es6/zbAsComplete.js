//自动补全
export default class zbAsComplete {
   constructor(config) {
        this.title = config.title ??= '自动补全';
        this.onSelected = config.onSelected ??= null; //选中后的回调
        this.isConfirm = 0; //是否已点击过确认按钮, 防止频繁点击
        this.params = config.params ??= {};//透传参数
        this.onShow = config.onShow ??= null;//显示弹窗前的回调
        this.onHide = config.onHide ??= null;//点击空白关闭弹窗时的回调
        this.data = config.data ??= []; //数据 [{content:''}]

        //浮层模板
        this.tpl = `<div id="zbAsComplete">
            <div id="zbCompleteMask"></div>
            <div id="zbCompleteBox">
              <div id="zbCompleteTitle"></div>
              <div id="zbCompleteBody">
                <input id="zbCompleteInput">
                <div id="zbCompleteList" class="zb-scroll-y"></dev>
              </div>
             </div>
            </div>`;

        this.init();    
    }        

    init () {
        //初始化css
        this.initCss();

        //初始模板, 绑定事件
        let body = document.getElementsByTagName('body')[0];
        let node = this.htmlToNode(this.tpl);
        node.querySelector('#zbCompleteMask').addEventListener('click', this.hide.bind(this));
        //node.querySelector('#zbCompleteInput').addEventListener('compositionend', this.changeInput.bind(this)); //输入全词以后才触发(手机浏览器不支持...)
        node.querySelector('#zbCompleteInput').addEventListener('input', this.changeInput.bind(this)); //输入部分单词就触发
        node.querySelector('#zbCompleteList').addEventListener('click', this.clicked.bind(this)); //输入部分单词就触发
        body.appendChild(node);
        
        //标题
        document.getElementById('zbCompleteTitle').innerText = this.title;

        return this;
    }

    htmlToNode (html) {
        let div = document.createElement('div');
        div.innerHTML = html;
        return div.firstElementChild;
    }

    //隐藏弹出层
    hide () {
        document.getElementById('zbAsComplete').remove(); //彻底移除了心静
        if (typeof this.onHide === 'function') {
            this.onHide();
        }
    }

    //显示弹出层
    show () {
        if (typeof this.onShow === 'function') {
            this.onShow();
        }
        document.getElementById('zbAsComplete').style.height = '100%';
    }

    //生成样式
    initCss() {
        let id = 'zbAsCompleteCss';
        if (document.getElementById(id)) {
            return;
        }
        let style = document.createElement('style');
        style.setAttribute('id', id);
        style.innerText =
            '.zb-scroll-y {overflow-y:scroll; white-space: nowrap;}'+
            '.zb-scroll-y::-webkit-scrollbar {display:none}'+
                        
            '#zbAsComplete{position:fixed;top:0;height:0;left:0;right:0;z-index:200;overflow:hidden;background-color:rgba(0,0,0,0.4);display:flex;flex-direction:column;justify-content:space-between}'+
            '#zbAsComplete #zbCompleteMask{height:100%}'+
            '#zbAsComplete #zbCompleteBox{max-height:90%;background-color:#fff;border-top-left-radius:6px;border-top-right-radius:6px;}'+
            '#zbAsComplete #zbCompleteTitle{height:40px;line-height:40px;font-size:20px;border-bottom:1px solid #eeeeee;text-align:center}'+
            '#zbAsComplete #zbCompleteBody{min-height:50px;padding:5px;}'+
            '#zbAsComplete #zbCompleteInput{height:30px; width:99%}'+
            '#zbAsComplete #zbCompleteList{max-height:800px;}'+
            '#zbAsComplete #zbCompleteList .zbarea-list-item{height:25px;margin:4px;border-bottom:1px solid #e8e8e8}'+
            '@keyframes bg_color {from{background:#eee;} to{background:#fff;}}'+
            '@-webkit-keyframes bg_color {from{background:#eee;} to{background:#fff;}}'
        ;

        let head = document.getElementsByTagName('head')[0];
        head.appendChild(style);
    }

    //覆盖指定id的dom元素
    replaceNode (id, node){
        let old = document.getElementById(id);
        let parent = old.parentNode;
        parent.replaceChild(node, old);
    }

    //输入字符后查询
    changeInput(obj){
        let domList = document.getElementById('zbCompleteList');
        let val = obj.target.value; 
        console.log(val);
        if (val.length === 0) {
            domList.innerHTML = '';
            return;//清空了输入框
        }
 
        let arr_word = val.split(' '); //空格分开的词求交集
        //找第一个词匹配的省市
        let list = [];
        for(let i=0; i<this.data.length;i++) {
            let row = this.data[i];
            if (row.content.indexOf(arr_word[0]) !== -1) {
                list.push(row);
            }
        }

        if (list.length === 0) {
            domList.innerHTML = '';
            return; //第一个词没找到就不用往下找了
        }

        for (let j=1; j<arr_word.length; j++ ) {
            let tmp = [];
            for (let k=0; k<list.length; k++) {
                if (list[k].content.indexOf(arr_word[k]) !== -1) {
                    tmp.push(list[k]);
                }
            }
            if (tmp.length === 0) {
                domList.innerHTML = '';
                return; //某个词没有匹配到, 直接退出, 不用再找了
            } else {
                list = tmp; //覆盖掉第一个词匹配的结果
            }
        }

        //渲染列表
        let tplListItem = '<div class="zbarea-list-item" data-params="{params}">{content}</div>';
        domList.innerHTML = this.repeatString(tplListItem, list, function(row){
            row.params = encodeURIComponent(JSON.stringify(row));
            return row;
        });

        //绑定事件
        // let items = document.getElementsByClassName('zbarea-list-item');
        // for (let i=0; i<items.length; i++) {
        //     items[i].addEventListener('click', this.clicked.bind(this)); //this是当前的zbAsComplete对象
        //     //items[i].addEventListener('click', this.clicked);
        // }

    }

    //记录点击的list值 onSelected
    clicked (e) {
        if (this.isConfirm === 1) {
            console.log('重复点击');
            return;
        }
        this.isConfirm = 1;

        let args = this.decodeObj(e.target.getAttribute('data-params'));
        
        this.hide();

        //调用用户自定义事件, 将参数传进去
        if (typeof this.onSelected === 'function') {
            this.onSelected(args, this.params);
        }

        return true;
    }

    //根据数组, 渲染HTML字符串
    repeatString (tplDom, arr, func=null) {
        if (!tplDom.length) {
            this.error('字符串长度为空');
            return;
        }

        if (arr.length === 0) {
            this.error('数据为空');
            return tplDom;
        }

        let tpl = tplDom;
        let out = '';
        for (let i=0; i<arr.length; i++) {
            if (typeof func === 'function') {
                arr[i] = func(arr[i]);
            }
            let map = arr[i];
            let tmp = tpl;
            for (let j in map) {
                let re = new RegExp('{' + j + '}', 'g');
                tmp = tmp.replace(re, map[j]);
            }

            let re = new RegExp('{_idx}', 'g');
            tmp = tmp.replace(re, parseInt(i)+1);

            out += tmp;
        }

        return out;
    };

    //是否是数组
    isArray (o){
        return Object.prototype.toString.call(o) === '[object Array]';
    }

    error(str) {
        console.log(str);
    }

    encodeObj (obj) {
        return encodeURIComponent(JSON.stringify(obj));
    }

    decodeObj (str) {
        return JSON.parse(decodeURIComponent(str));
    }

}
/**
 * 用法举例:
let cmp = new zbAsComplete({
    title: '自动补全',
    params: {a:1, b:2}, //透传参数
    onSelected: function(selected, params){
        console.log(selected); //选中的数据
        console.log(params); //透传参数
    },
    data: [{content:'aaa'},{content:'bbb'},{content:'ccc'}]
});
cmp.show();
 */
