//单选列表
export default class zbAsRadio {
    constructor(config) {
        this.title = config.title ??= '单选';
        this.item_class = config.item_class ??= 'item_center';//所有数据
        this.item_img_class = config.item_img_class ??= 'item_img';//所有数据
        this.isConfirm = 0; //是否已点击过确认按钮, 防止频繁点击
        this.onSelected = config.onSelected ??= null; //点击确认后执行回调
        this.onShow = config.onShow ??= null;//显示弹窗前的回调
        this.onHide = config.onHide ??= null;//点击空白关闭弹窗时的回调
        this.data = config.data ??= null; //选项
        this.params = config.params ??= {};//透传参数

        //浮层模板
        this.tpl = `<div id="zbAsRadio">
            <div id="zbRadioMask" ></div>
            <div id="zbRadioBox">
              <div id="zbRadioTitle"></div>
              <div id="zbRadioBody" class="zb-scroll-y"></div>
             </div>
             <input type="hidden" id="zbRadioInput" value="">
            </div>`;

        this.init();
    }
    
    init () {
        let flag = document.getElementById('zbAsRadio');
        if (flag === null) {
            //初始化css
            this.initCss();

            //初始模板, 绑定事件
            let body = document.getElementsByTagName('body')[0];
            let node = this.htmlToNode(this.tpl);
            node.querySelector('#zbRadioMask').addEventListener('click', this.hide.bind(this));
            body.appendChild(node);
        }

        //标题
        document.getElementById('zbRadioTitle').innerText = this.title;
        this.addList(this.data);

        return this;
    }

    htmlToNode (html) {
        let div = document.createElement('div');
        div.innerHTML = html;
        return div.firstElementChild;
    }

    //隐藏弹出层
    hide () {
        document.getElementById('zbAsRadio').remove(); //彻底移除了心静
        if (typeof this.onHide === 'function') {
            this.onHide();
        }
    }

    //显示弹出层
    show () {
        if (typeof this.onShow === 'function') {
            this.onShow();
        }
        document.getElementById('zbAsRadio').style.height = '100%';
    }

    //生成样式
    initCss() {
        let id = 'zbAsRadioCss';
        if (document.getElementById(id)) {
            return;
        }
        let style = document.createElement('style');
        style.setAttribute('id', id);
        style.innerText =
            '.zb-scroll-x {overflow-x:scroll; white-space: nowrap;}'+
            '.zb-scroll-x::-webkit-scrollbar {display:none}'+
            '.zb-scroll-y {overflow-y:scroll; white-space: nowrap;}'+
            '.zb-scroll-y::-webkit-scrollbar {display:none}'+
            '.zb-scroll{overflow:scroll; white-space: nowrap;}'+
            '.zb-scroll::-webkit-scrollbar {display:none}'+
            

            '#zbAsRadio{position:fixed;top:0;height:0;left:0;right:0;z-index:200;overflow:hidden;background-color:rgba(0,0,0,0.4); display:flex;flex-direction:column;justify-content:space-between;}'+
            '#zbAsRadio #zbRadioMask{height:100%}'+
            '#zbAsRadio #zbRadioBox {max-height:90%;background-color:#fff;border-top-left-radius:6px;border-top-right-radius:6px;}'+
            '#zbAsRadio #zbRadioTitle {height:40px;line-height:40px;font-size:20px;border-bottom:1px solid #eeeeee; text-align:center}'+
            '#zbAsRadio #zbRadioBody {min-height:50px;max-height:500px; padding:3px;}'+
            '#zbAsRadio #zbRadioBody .zbas-list-item {padding:3px;height:30px;border-bottom:1px solid #e8e8e8}'+
            '#zbAsRadio #zbRadioBody .item_center{display:flex; flex-direction:row;justify-content: center; align-items: center;}'+
            '#zbAsRadio #zbRadioBody .item_left{display:flex; flex-direction:row;justify-content: left; align-items: center;}'+
            '#zbAsRadio #zbRadioBody .item_right{display:flex; flex-direction:row;justify-content: right; align-items: center;}'+
            '#zbAsRadio #zbRadioBody .item_img{height:30px;width:30px;margin-right:5px;}'+
            '@keyframes bg_color {from{background:#eee;} to{background:#fff;}}'+
            '@-webkit-keyframes bg_color {from{background:#eee;} to{background:#fff;}}'
        ;

        let head = document.getElementsByTagName('head')[0];
        head.appendChild(style);
    }

    //覆盖指定id的dom元素
    replaceNode (id, node){
        let old = document.getElementById(id);
        let parent = old.parentNode;
        parent.replaceChild(node, old);
    }

    /**
     * 添加纵向列表数据
     */
    addList() {
        let data = this.data;
        let item_class = this.item_class;
        let item_img_class = this.item_img_class;

        let tplListItem = '<div class="zbas-list-item {item_class}"" data-index="{idx}">{title}</div>';
        let tplListItemImg = '<div class="zbas-list-item {item_class}" data-index="{idx}"><img class="{item_img_class}" src="{img}">{title}</div>';

        let dataBody = document.getElementById('zbRadioBody');
        let strItems = '';
        for (let i=0; i<data.length; i++) {
            let item = data[i];
            if (this.isObject(item)) {
                // [{title:'aaa', value:'aaa', img:'', a:1, b2}]
                let tpl = (item.img !== undefined) ? tplListItemImg : tplListItem;
                strItems += this.repeatString(tpl, [item], function(row){
                    row.item_class = item_class;
                    row.item_img_class = item_img_class;
                    row.idx = i;
                    return row;
                });
            } else {
                //[1,2,3]
                strItems += this.repeatString(tplListItem, [item], function(v){
                    let row = {title: v, value: v, idx:i};
                    row.item_class = item_class;
                    return row;
                });
            }
            
        }

        //渲染列表
        dataBody.innerHTML = strItems;

        //添加事件绑定
        let items = dataBody.getElementsByClassName('zbas-list-item');
        for (let i=0; i<items.length; i++) {
            items[i].addEventListener('click', this.click.bind(this));
        }

        return this;
    }

    //记录点击的list值
    click (e) {
        let obj = e.target;        
        if (this.isConfirm === 1) {
            return;
        } else {
            this.isConfirm = 1;
        }

        if (typeof this.onSelected === 'function') {
            let idx = obj.getAttribute('data-index');
            let d = this.data[idx];
            this.hide();
            this.onSelected(d, this.params);
        }
        
        return true;
    }
    
    //根据数组, 渲染HTML字符串
    repeatString (tplDom, arr, func=null) {
        if (!tplDom.length) {
            this.error('字符串长度为空');
            return;
        }

        if (arr.length === 0) {
            this.error('数据为空');
            return tplDom;
        }

        let tpl = tplDom;
        let out = '';
        for (let i=0; i<arr.length; i++) {
            if (typeof func === 'function') {
                arr[i] = func(arr[i]);
            }
            let map = arr[i];
            let tmp = tpl;
            for (let j in map) {
                let re = new RegExp('{' + j + '}', 'g');
                tmp = tmp.replace(re, map[j]);
            }

            let re = new RegExp('{_idx}', 'g');
            tmp = tmp.replace(re, parseInt(i)+1);

            out += tmp;
        }

        return out;
    };

    //是否是数组
    isArray (o){
        return Object.prototype.toString.call(o) === '[object Array]';
    }

    isObject (o) {
        return Object.prototype.toString.call(o) === '[object Object]';
    }

    error(str) {
        console.log(str);
    }
}
/**
 * 用法举例:
let radio = new zbAsRadio({
    title: '单选',
    data: [
        {title:'a', value:1, img:'summer/icons/right-arrow.png'},
        {title:'b', value:2, img:'summer/icons/right-arrow.png'},
        {title:'c', value:3, img:'summer/icons/right-arrow.png'},
        {title:'d', value:4, img:'summer/icons/right-arrow.png'},
        {title:'e', value:5}
    ],
    item_class: 'item_left', //可选 item_center,item_left,item_right
    item_img_class: 'item_img',
    params: {a:1, b:2}, //透传参数
    onSelected: function(data, params) {
        console.log(data); //被点击的数据
        console.log(params); //透传参数
    },
    onShow: function(){},
    onHide: function(){},

});
radio.show();
 */
