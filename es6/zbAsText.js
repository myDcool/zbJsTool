export default class zbAsText {
    constructor(config) {
        this.title = config.title ??= '输入文本';
        this.config = config.config ??= {}; //配置
        this.params = config.params ??= null;//透传参数
        this.isConfirm = 0; //是否已点击过确认按钮, 防止频繁点击
        this.onConfirm = config.onConfirm ??= null; //点击确认后执行回调
        this.onShow = config.onShow ??= null;//显示弹窗前的回调
        this.onHide = config.onHide ??= null;//点击空白关闭弹窗时的回调

        //浮层模板
        this.tpl = `<div id="zbAsText">
            <div id="zbTextMask"></div>
            <div id="zbTextBox">
              <div id="zbTextTitle"></div>
              <div id="zbTextBody"></div>
              <div id="zbTextFooter">
               <div class="zbTextBtn" id="zbTextBtnCancel">取消</div>
               <div class="zbTextBtn" id="zbTextBtnConfirm">确认</div>
              </div>
             </div>
             <input type="hidden" id="zbTextInput" value="">
            </div>`;

        this.init();
    }

    //初始化
    init () {
        //初始化css
        this.initCss();
        
        //初始模板, 绑定事件
        let body = document.getElementsByTagName('body')[0];
        let node = this.htmlToNode(this.tpl);
        let that = this;
        node.addEventListener('click', function(e){
            // console.log(e)
            // console.log(e.srcElement)
            // console.log(e.target);
            if (e.target.id === 'zbTextMask' || e.target.id === 'zbTextBtnCancel') {
                that.hide();
            } else if (e.target.id === 'zbTextBtnConfirm') {
                that.confirm();
            }
        });
        body.appendChild(node);

        //标题
        document.getElementById('zbTextTitle').innerText = this.title;
        let input_type = this.config.type ??= 'text';
        if (input_type === 'textarea') {
            let value = this.config.value;
            delete this.config.value;
            this.addTextarea({attr:this.config, value: value});
        } else {
            this.addInput({attr:this.config});
        }

        return this;
    }

    htmlToNode (html) {
        let div = document.createElement('div');
        div.innerHTML = html;
        return div.firstElementChild;
    }

    //隐藏弹出层
    hide () {
        document.getElementById('zbAsText').remove(); //彻底移除了心静
        if (typeof this.onHide === 'function') {
            this.onHide();
        }
    }

    //显示弹出层
    show () {
        if (typeof this.onShow === 'function') {
            this.onShow();
        }
        document.getElementById('zbAsText').style.height = '100%';
    }

    //点击确认按钮触发执行
    confirm () {
        if (this.isConfirm === 1) {
            console.log('重复点击');
            return;
        }
        this.isConfirm = 1;
        let input = document.getElementById('zbTextInput');
        
        this.hide();
        if (typeof this.onConfirm === 'function') {
            this.onConfirm(input.value, this.params);
        }
    }

    //生成样式
    initCss() {
        let id = 'zbAsTextCss';
        if (document.getElementById(id)) {
            return;
        }
        let style = document.createElement('style');
        style.setAttribute('id', id);
        style.innerText =
            '#zbAsText{position:fixed;top:0;height:0;left:0;right:0;z-index:200;overflow:hidden;background-color:rgba(0,0,0,0.4); display:flex;flex-direction:column;justify-content:space-between}'+
            '#zbAsText #zbTextMask{height:100%}'+
            '#zbAsText #zbTextBox{max-height:90%;background-color:#fff;border-top-left-radius:6px;border-top-right-radius:6px;}'+
            '#zbAsText #zbTextTitle{height:40px;line-height:40px;font-size:20px;border-bottom:1px solid #eeeeee; text-align:center}'+
            '#zbAsText #zbTextBody{min-height:100px; padding:5px;}'+
            '#zbAsText #zbTextBody .zb-input{height:30px; width:99%}'+
            '#zbAsText #zbTextBody #char-stat{width:100%; text-align:right}'+
            '#zbAsText #zbTextFooter{padding:5px;border-top:1px solid #eeeeee;height:40px;line-height:40px; display:flex; flex-direction:row; justify-content:center}'+
            '#zbAsText #zbTextFooter .zbTextBtn{width:50%;font-size:20px;text-align:center;}'+
            '@keyframes bg_color {from{background:#eee;} to{background:#fff;}}'+
            '@-webkit-keyframes bg_color {from{background:#eee;} to{background:#fff;}}'
        ;

        let head = document.getElementsByTagName('head')[0];
        head.appendChild(style);
    }

    //覆盖指定id的dom元素
    replaceNode (id, node){
        let old = document.getElementById(id);
        let parent = old.parentNode;
        parent.replaceChild(node, old);
    }

    //添加文字信息, 文本，日期，数字
    addInput (config) {
        //console.log(config);
        let box = document.getElementById('zbTextBody');
        
        //初始化input标签
        let attrId = 'zb-input';
        let input = document.createElement('input');
        input.setAttribute('id', attrId);
        for (let k in config.attr) {
            if (k === 'id') {continue}
            input.setAttribute(k, config.attr[k]);
            if (k == 'value') {
                document.getElementById('zbTextInput').value = config.attr[k];
            }
        }

        input.addEventListener('input', this.changeTextParams.bind(this, attrId));
        input.classList.add('zb-input');
        box.appendChild(input);

        //初始化计数器
        let maxLength = config['attr']['maxlength'] ? config['attr']['maxlength'] : 0;
        if (maxLength > 0) {
            let defaultVal = document.getElementById('zbTextInput').value;
            let defaultLength = defaultVal.length ??= 0;
            let tpl = `<div id="char-stat">${defaultLength} / ${maxLength}</div>`;
            let node = this.htmlToNode(tpl);
            box.appendChild(node);
        }

        return this;
    }

    //添加文字信息, 可换行
    addTextarea (config) {
        let box = document.getElementById('zbTextBody');
        box.style.height = '500px';

        //初始化textarea标签
        let attrId = 'zb-textarea';
        let input = document.createElement('textarea');
        input.setAttribute('id', attrId);
        for (let k in config['attr']) {
            if (k === 'id') {continue};
            input.setAttribute(k, config['attr'][k]);
        }
        
        input.addEventListener('input', this.changeTextParams.bind(this, attrId));
        input.setAttribute('style', 'height:450px;width:99%;padding:5px;');
        input.innerText = config['value'] ??= '';
        box.appendChild(input);

        document.getElementById('zbTextInput').value = config['value'] ??= '';

        //初始化计数器
        let maxLength = config['attr']['maxlength'] ? config['attr']['maxlength'] : 0;
        if (maxLength > 0) {
            let defaultVal = document.getElementById('zbTextInput').value;
            let defaultLength = defaultVal.length ??= 0;
            let tpl = `<div id="char-stat">${defaultLength} / ${maxLength}</div>`;
            let node = this.htmlToNode(tpl);
            box.appendChild(node);
        }

        return this;
    }

    //输入文本框时, 记录下输入的值, 并实时计算字数
    changeTextParams (id){
        let obj = document.getElementById(id);
        //变更长度
        let maxLength = obj.getAttribute('maxlength');
        if (maxLength) {
            let currLen = obj.value.length;
            
            if (currLen > maxLength) {
                obj.value = obj.value.substring(0, maxLength);
                currLen = maxLength;
            }

            document.getElementById('char-stat').innerText = `${currLen} / ${maxLength}`;
        }
        
        document.getElementById('zbTextInput').value = obj.value;
    }

    //是否是数组
    isArray (o){
        return Object.prototype.toString.call(o) === '[object Array]';
    }

    error(str) {
        console.log(str);
    }

    encodeObj (obj) {
        return encodeURIComponent(JSON.stringify(obj));
    }

    decodeObj (str) {
        return JSON.parse(decodeURIComponent(str));
    }
}
/**
 * 用法举例:
let input = new zbAsText({
    title: '请输入文本',
    params: {a:1, b:2}, //透传参数
    // config: {id:'test',  type:'number',      value: 0, maxlength: 5, step: 1,autocomplete:'off', autofocus:true},
    // config: {id:'test',  type:'number',      value: 0, maxlength: 5, step: 0.1,autocomplete:'off',},
    // config: {id:'test',  type:'text',        value: 'aaa', maxlength: 10, autocomplete:'off',},
    // config: {id:'test',  type:'date',        value: '2023-01-01', maxlength: 10, autocomplete:'off',},
    // config: {id:'test',  type:'datetime-local', value: '2023-01-01T00:00:00', min: '1900-01-01T00:00:00', step:1,  autocomplete:'off',},
    // config: {id:'test',  type:'email',       value: '', maxlength:100,  autocomplete:'off',},
    config: {id:'test',  type:'textarea',   value: '1111', maxlength:300,  autocomplete:'off',},

    onConfirm: function (data, params) {
        console.log(data);
        console.log(params);
    },
    onShow: function(){},
    onHide: function(){},
});
input.show();
 */
