// 发送http请求
export default class zbFetch {
    constructor(config={}) {
        this.headers = config.headers ??=  {
            'Content-Type': 'application/json',
            //'Content-Type': 'application/x-www-form-urlencoded'
            //'user-agent': "Mozilla/4.0 MDN Example",
        }
        
        this.mode = 'cors';
        this.cache = 'default';
        this.credentials = 'same-origin';
        this.redirect = 'follow';
        this.referrer = 'client';
        this.referrerPolicy = 'origin';
        this.defaultParams = {};

    }

    setDefaultParams(params) {
        this.defaultParams = params;
    }

    post (url, param, callback=null) {
        param.defaultParams = this.defaultParams;
        fetch(url,{
            method:'POST', 
            headers:this.headers, 
            mode:this.mode, 
            cache:this.cache, 
            credentials:this.credentials, 
            redirect:this.redirect, 
            referrer:this.referrer, 
            referrerPolicy:this.referrerPolicy,
            body: JSON.stringify(param)
        })
        .then((res) => res.json())
        .then((data) => this.handleResponse(data, callback))
    }

    get (url, callback=null) {
        fetch(url,{
            method:'GET', 
            headers:this.headers, 
            mode:this.mode, 
            cache:this.cache, 
            credentials:this.credentials, 
            redirect:this.redirect, 
            referrer:this.referrer, 
            referrerPolicy:this.referrerPolicy
        })
        .then((res) => res.json())
        .then((data) => this.handleResponse(data, callback));
    }
    
    handleResponse (rs, callback) {
        if (rs) {
            if (rs.url && rs.is_redirect === 1) {
                location.href = rs.url;
                return true;
            }

            if (rs.code === 1 || rs.code === '1') {
                if (typeof callback === 'function') {
                    callback(rs.data ??= {});
                    return true;
                }
            } else {
                this.error(rs.msg);
                return false;
            }
        } else {
            this.error('未能解析返回数据');
            return false;
        }
    }

    error(msg) {
        alert(msg);
    }

    //获取时间戳
    getTimestamp(){
        let now = (new Date()).getTime();
        now = now.toString().slice(0, -3);
        return parseInt(now);
    }

    
}

/**
 * 
用法举例:
let req = new zbFetch();
req.post('http://www.test7.com/test.php', {a:1, b:2}, function(rs){console.log(rs);});
req.get('http://www.test7.com/test.php', function(rs){console.log(rs);});

 */
