export default class zbImage {

    constructor() {
        this.canvas = null;
    }
    

    setCanvas (canvas) {
        this.canvas = canvas;
    }

    createQRCanvas (QRText, targetId) {
        //生成商品二维码
        $('#'+targetId).qrcode({text: QRText, quiet: 2});
        let QRCanvas = document.getElementById(targetId).getElementsByTagName('canvas')[0];

    };

    /**
     * 将以base64的图片url数据转换为Blob
     * @param urlData 用url方式表示的base64图片数据
     */
    convertBase64UrlToBlob(urlData){
        let arr = urlData.split(','), mime = arr[0].match(/:(.*?);/)[1],
            bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
        while(n--){
            u8arr[n] = bstr.charCodeAt(n);
        }
        return new Blob([u8arr], {type:mime});
    }

    //把二维码添加到图片的右下角,并对图片进行圆角处理
    //依赖jquery jquery-qrcode
    addQRToImage (imageUrl, QRText, maxWidth, parentId, imageClass) {

        let image = new Image();
        image.setAttribute('crossOrigin', 'anonymous');
        image.onload = function () {
            //创建画布
            let canvasElement = document.createElement('canvas');
            let ctx = canvasElement.getContext('2d');

            //将图片画到canvas上去
            let imageW = image.width;
            let imageH = image.height;
            let w = imageW;
            let h = imageH;
            if (imageW > maxWidth) {
                w = maxWidth;
                h = parseInt((maxWidth * imageH) / imageW); //高度等比缩放
            }
            canvasElement.setAttribute('width', w+'px');
            canvasElement.setAttribute('height', h+'px');

            let r = 10; //半径
            let r2 = r/2;
            let x = 0; //起始x坐标
            let y = 0; //起始y坐标
            ctx.save();
            ctx.beginPath(); // draw top and top right corner
            ctx.moveTo(x + r, y);
            ctx.arcTo(x + w, y, x + w, y + r, r); // draw right side and bottom right corner
            ctx.arcTo(x + w, y + h, x + w - r, y + h, r); // draw bottom and bottom left corner
            ctx.arcTo(x, y + h, x, y + h - r, r); // draw left and top left corner
            ctx.arcTo(x, y, x + r, y, r);
            //ctx.fill();
            //ctx.strokeStyle="gray";
            //ctx.stroke();
            ctx.clip();

            ctx.drawImage(image, 0, 0, w, h);

            //生成商品二维码
            //$('#QR').qrcode({text:QRText,quiet:3,mode:2,mSize: 0.08,mPosX:0.5,mPosY:1.01,label:'收藏可优惠￥0.5'});
            $('#QR').qrcode({text:QRText,quiet:2});
            let canvasQR = document.getElementById('QR').getElementsByTagName('canvas')[0];

            //将二维码画上去
            let qrWidth = 95;
            ctx.drawImage(canvasQR, w-qrWidth-r2, h-qrWidth-r2, qrWidth, qrWidth);

            //将图片追加到制定容器中
            let img = document.createElement('img');
            img.src = canvasElement.toDataURL('image/png');
            img.setAttribute('class', imageClass);
            let box = document.getElementById(parentId);
            box.innerHTML = '';
            box.appendChild(img);
            //box.style.display = 'block';
        };

        image.src = imageUrl;
    }

    //将二维码拼接在图片右边, 并加一行文字在下方
    //依赖jquery jquery-qrcode
    addQRTextToImage (mainImageUrl, qrText, text, parentId, imageClass) {
        let image = new Image();
        image.setAttribute('crossOrigin', 'anonymous');
        image.onload = function () {
            //创建画布
            let canvasElement = document.createElement('canvas');
            let ctx = canvasElement.getContext('2d');

            //将画布改为圆角
            let imageW = image.width;
            let imageH = image.height;
            let maxItemWidth = 160;
            let textHeight = 24;
            let w = maxItemWidth * 2; //横向两个元素
            let h = maxItemWidth + textHeight; // 垂直图片+文字

            canvasElement.setAttribute('width', w+'px');
            canvasElement.setAttribute('height', h+'px');

            //将画布变为圆角
            let r = 10; //半径
            ctx.beginPath();
            ctx.moveTo(r, 0); //左上角弧度,右结束点
            ctx.lineTo(w-r, 0); //画水平直线,到右上角弧度的开始点
            ctx.arcTo(w, 0, w, r, r); //画右上角弧度
            ctx.lineTo(w, h-r); //画垂直线, 到右下角的弧度开始点
            ctx.arcTo(w, h, w-r, h, r); // 画右下角弧度
            ctx.lineTo(r, h); // 画水平线, 到左下角弧度开始点
            ctx.arcTo(0, h, 0, h - r, r); //画左下角弧度
            ctx.lineTo(0, r); //画垂直线, 到左上角弧度开始点
            ctx.arcTo(0, 0, r, 0, r); // 画左上角弧度
            //ctx.strokeStyle="rgba(0,0,0,0.1)";
            //ctx.stroke();
            ctx.fillStyle= '#ffffff';
            ctx.fill();
            ctx.clip();

            //把图像剪切成 imageW*imageW, 然后缩放为 maxItemWidth, 从画布的0,0处开始渲染
            ctx.drawImage(image, 0, 0, imageW, imageW, 0, 0, maxItemWidth, maxItemWidth);

            //生成商品二维码
            $('#QR').qrcode({text:qrText,quiet:1});
            let canvasQR = document.getElementById('QR').getElementsByTagName('canvas')[0];

            //将二维码画到右边
            ctx.drawImage(canvasQR, maxItemWidth, 0, maxItemWidth, maxItemWidth);

            //在二维码上边写上文字
            //ctx.shadowBlur=2;
            //ctx.shadowColor="rgba(0,0,0,0.1)";
            ctx.font=text['font'];
            ctx.fillStyle = text['color'];
            let textTotalW = ctx.measureText(text['text']).width;
            let startTxtX = (w - textTotalW) / 2;
            ctx.fillText(text['text'],startTxtX, h-5, maxItemWidth);

            //将图片加入到指定的容器中
            let img = document.createElement('img');
            img.src = canvasElement.toDataURL('image/png');
            img.setAttribute('class', imageClass);
            let box = document.getElementById(parentId);
            box.innerHTML = '';
            box.appendChild(img);
            //box.style.display = 'block';
        };

        image.src = mainImageUrl;
    }

    //将二维码拼接在图片右边, 文字在二维码上方, 传入图片的URL
    //依赖jquery jquery-qrcode
    addQRTextToImage1 (mainImageUrl, qrText, text, parentId, imageClass) {
        let image = new Image();
        image.setAttribute('crossOrigin', 'anonymous');
        image.onload = function () {
            //创建画布
            let canvasElement = document.createElement('canvas');
            let ctx = canvasElement.getContext('2d');

            //将画布改为圆角
            let imageW = image.width;
            let imageH = image.height;
            let maxItemWidth = 300;
            let w = maxItemWidth * 2; //横向两个元素
            let h = maxItemWidth // 垂直高度

            canvasElement.setAttribute('width', w+'px');
            canvasElement.setAttribute('height', h+'px');

            //将画布变为圆角
            let r = 10; //半径
            ctx.beginPath();
            ctx.moveTo(r, 0); //左上角弧度,右结束点
            ctx.lineTo(w-r, 0); //画水平直线,到右上角弧度的开始点
            ctx.arcTo(w, 0, w, r, r); //画右上角弧度
            ctx.lineTo(w, h-r); //画垂直线, 到右下角的弧度开始点
            ctx.arcTo(w, h, w-r, h, r); // 画右下角弧度
            ctx.lineTo(r, h); // 画水平线, 到左下角弧度开始点
            ctx.arcTo(0, h, 0, h - r, r); //画左下角弧度
            ctx.lineTo(0, r); //画垂直线, 到左上角弧度开始点
            ctx.arcTo(0, 0, r, 0, r); // 画左上角弧度
            //ctx.strokeStyle="rgba(0,0,0,0.1)";
            //ctx.stroke();
            ctx.fillStyle= '#f8f8f8';
            ctx.fill();
            ctx.clip();

            //把图像剪切成 imageW*imageW, 然后缩放为 maxItemWidth, 从画布的0,0处开始渲染
            ctx.drawImage(image, 0, 0, imageW, imageW, 0, 0, maxItemWidth, maxItemWidth);

            //生成商品二维码
            $('#QR').qrcode({text:qrText,quiet:1});
            let canvasQR = document.getElementById('QR').getElementsByTagName('canvas')[0];

            //将二维码画到右边
            let qrWidth = 110;
            ctx.drawImage(canvasQR, w-qrWidth-5, h-qrWidth-5, qrWidth, qrWidth);

            //在下边写上文字
            ctx.font=text['font'];
            ctx.fillStyle = text['color'];
            let txtFontSize = text['font-size'];
            let startTxtX = maxItemWidth + 20;
            let startTxtY = txtFontSize;
            for (let i=0; i<text['text'].length; i++) {
                ctx.fillText(text['text'][i], startTxtX, startTxtY, maxItemWidth -20);
                startTxtY += txtFontSize;
            }

            //将图片加入到指定的容器中
            let img = document.createElement('img');
            img.src = canvasElement.toDataURL('image/png');
            img.setAttribute('class', imageClass);
            let box = document.getElementById(parentId);
            box.innerHTML = '';
            box.appendChild(img);
            //box.style.display = 'block';
        };

        image.src = mainImageUrl;
    }

    //将二维码拼接在图片右边, 文字在二维码上方, 读取表单中的图片
    //依赖jquery jquery-qrcode
    addQRTextToImage2 (inputFileId, qrText, text, parentId, imageClass) {

        let fileInput = document.getElementById(inputFileId);
        let objImage = fileInput.files[0];

        if (!objImage.size) {
            return false;
        }

        let reader = new FileReader();
        reader.onload = function(evt) {

            let image = new Image();
            image.setAttribute('crossOrigin', 'anonymous');
            image.onload = function () {
                //创建画布
                let canvasElement = document.createElement('canvas');
                let ctx = canvasElement.getContext('2d');

                //将画布改为圆角
                let imageW = image.width;
                let imageH = image.height;
                let maxItemWidth = imageW > 400 ? 400 : imageW;
                let maxItemHight = Math.round((imageH*maxItemWidth) / imageW);
                let w = maxItemWidth * 2; //横向两个元素
                let h = maxItemHight // 垂直高度

                canvasElement.setAttribute('width', w+'px');
                canvasElement.setAttribute('height', h+'px');

                //将画布变为圆角
                let r = 10; //半径
                ctx.beginPath();
                ctx.moveTo(r, 0); //左上角弧度,右结束点
                ctx.lineTo(w-r, 0); //画水平直线,到右上角弧度的开始点
                ctx.arcTo(w, 0, w, r, r); //画右上角弧度
                ctx.lineTo(w, h-r); //画垂直线, 到右下角的弧度开始点
                ctx.arcTo(w, h, w-r, h, r); // 画右下角弧度
                ctx.lineTo(r, h); // 画水平线, 到左下角弧度开始点
                ctx.arcTo(0, h, 0, h - r, r); //画左下角弧度
                ctx.lineTo(0, r); //画垂直线, 到左上角弧度开始点
                ctx.arcTo(0, 0, r, 0, r); // 画左上角弧度
                //ctx.strokeStyle="rgba(0,0,0,0.1)";
                //ctx.stroke();
                ctx.fillStyle= '#f8f8f8';
                ctx.fill();
                ctx.clip();

                //把图像剪切成 imageW*imageW, 然后缩放为 maxItemWidth, 从画布的0,0处开始渲染
                ctx.drawImage(image, 0, 0, imageW, imageH, 0, 0, maxItemWidth, maxItemHight);

                //生成商品二维码
                $('#QR').qrcode({text:qrText,quiet:1});
                let canvasQR = document.getElementById('QR').getElementsByTagName('canvas')[0];

                //将二维码画到右边
                let qrWidth = Math.round(maxItemWidth / 3);
                ctx.drawImage(canvasQR, w-qrWidth-5, h-qrWidth-5, qrWidth, qrWidth);

                //在下边写上文字
                ctx.font=text['font'];
                ctx.fillStyle = text['color'];
                let txtFontSize = text['font-size'];
                let startTxtX = maxItemWidth + 20;
                let startTxtY = txtFontSize;
                for (let i=0; i<text['text'].length; i++) {
                    ctx.fillText(text['text'][i], startTxtX, startTxtY, maxItemWidth -20);
                    startTxtY += txtFontSize;
                }

                //将图片加入到指定的容器中
                let img = document.createElement('img');
                img.src = canvasElement.toDataURL('image/png');
                img.setAttribute('class', imageClass);
                let box = document.getElementById(parentId);
                box.innerHTML = '';
                box.appendChild(img);
            }

            image.src = evt.target.result;

        };

        reader.readAsDataURL(objImage);
    }

    QRText (qrText, text, parentId, imageClass) {

        //创建画布
        let canvasElement = document.createElement('canvas');
        let ctx = canvasElement.getContext('2d');

        //将画布改为圆角
        let w = 500; //横向两个元素
        let h = 300; // 垂直高度

        canvasElement.setAttribute('width', w+'px');
        canvasElement.setAttribute('height', h+'px');

        //将画布变为圆角
        let r = 10; //半径
        ctx.beginPath();
        ctx.moveTo(r, 0); //左上角弧度,右结束点
        ctx.lineTo(w-r, 0); //画水平直线,到右上角弧度的开始点
        ctx.arcTo(w, 0, w, r, r); //画右上角弧度
        ctx.lineTo(w, h-r); //画垂直线, 到右下角的弧度开始点
        ctx.arcTo(w, h, w-r, h, r); // 画右下角弧度
        ctx.lineTo(r, h); // 画水平线, 到左下角弧度开始点
        ctx.arcTo(0, h, 0, h - r, r); //画左下角弧度
        ctx.lineTo(0, r); //画垂直线, 到左上角弧度开始点
        ctx.arcTo(0, 0, r, 0, r); // 画左上角弧度
        // //ctx.strokeStyle="rgba(0,0,0,0.1)";
        // //ctx.stroke();
        ctx.fillStyle= '#fff';
        ctx.fill();
        ctx.clip();

        //生成二维码
        $('#QR').qrcode({text:qrText,quiet:2});
        let canvasQR = document.getElementById('QR').getElementsByTagName('canvas')[0];

        //将二维码画到右边
        let qrWidth = h;
        ctx.drawImage(canvasQR, 0, 0, qrWidth, qrWidth);

        //在下边写上文字
        ctx.font=text['font'];
        ctx.fillStyle = text['color'];
        let rowGap = parseInt(text['row_gap']);
        let startTxtX = h;
        let startTxtY = parseInt(text['font_size']) + 10;
        for (let i=0; i<text['text'].length; i++) {
            ctx.fillText(text['text'][i], startTxtX, startTxtY, 200-10);
            startTxtY += rowGap;
        }

        //将图片加入到指定的容器中
        let img = document.createElement('img');
        img.src = canvasElement.toDataURL('image/png');
        img.setAttribute('class', imageClass);
        let box = document.getElementById(parentId);
        box.innerHTML = '';
        box.appendChild(img);
    }

    /**
     * 创建建二维码图片
     * @param qrText 文字
     * @param parentId 父级元素ID
     * @param w 二维码宽度
     * @param h 二维码高度
     */
    createQR (qrText, parentId, w=255, h=255) {
        //生成二维码
        //https://www.cnblogs.com/gygang/p/9116140.html
        $('#'+parentId).qrcode({
            render:'canvas',
            width: w,
            height: h,
            quiet:2,
            text:qrText,
            });
    }

    //显示覆层
    showMask (objImg) {
        let css = '#mask_image_box {position: fixed;top: 0;left: 0;bottom: 0;right: 0;padding-top: 16px;overflow: hidden;background-color: rgba(0,0,0,0.3);display:none;z-index: 200;}'
            +'#mask_image_box #mask_image {width:340px;position: absolute;top: 50%;left: 50%;transform: translate(-50%,-50%);}'
        let maskCss = document.getElementById('mask_image_box');
        if (!maskCss) {
            let style = document.createElement('style');
            style.innerText = css;

            let img = document.createElement('img');
            img.setAttribute('id', 'mask_image');

            let div = document.createElement('div');
            div.setAttribute('id', 'mask_image_box');
            div.setAttribute('onclick', "this.style.display='none'; return false;");

            div.appendChild(img);

            let body = document.getElementsByTagName('body')[0]
            body.appendChild(style);
            body.appendChild(div);

        }

        let imgSrc = objImg.getAttribute('src');
        document.getElementById('mask_image').setAttribute('src', imgSrc);
        document.getElementById('mask_image_box').style.display = 'block';
    }
}
