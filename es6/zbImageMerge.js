/**
 * 图片处理, 依赖jquery
 **/
export default class zbImageMerge {
    constructor(){
        this.canvas = null;
    }
    

    setCanvas (canvas) {
        this.canvas = canvas;
    }

    createQRCanvas (QRText, targetId) {
        //生成商品二维码
        $('#'+targetId).qrcode({text: QRText, quiet: 2});
        let QRCanvas = document.getElementById(targetId).getElementsByTagName('canvas')[0];

    };

    /**
     * 将以base64的图片url数据转换为Blob
     * @param urlData 用url方式表示的base64图片数据
     */
    convertBase64UrlToBlob(urlData){
        let arr = urlData.split(','), mime = arr[0].match(/:(.*?);/)[1],
            bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
        while(n--){
            u8arr[n] = bstr.charCodeAt(n);
        }
        return new Blob([u8arr], {type:mime});
    }

    //从input选择的图片文件中读取到img对象中
    getImageByFile(file, callback) {
        let p = new Promise((success, error) => {
            let reader = new FileReader();
            reader.onload = function(evt) {
                let image = new Image();
                image.src = evt.target.result;
                success(image);
            };

            reader.readAsDataURL(file);
        }).then(image => {
            callback(image);
        }).catch(rs => {
            console.log('getImageByFile: error: ' + rs);
        })
            
    }

    /**
     * image: 原始图片
     * x: 开始x坐标
     * y: 开始y坐标 
     */
    cut(image, x, y, w, h) {
        console.log(image);
        if (!image.naturalWidth) {
            console.log('没有获取到图片信息');
            return false;
        }

        if (!x) {x = 0;}
        if (!y) {y = 0;}
        if (!w) {w = image.naturalWidth;}
        if (!h) {h = image.naturalHeight;}


        //创建画布
        let canvas = document.createElement('canvas');
        let ctx = canvas.getContext('2d');
        canvas.width  = w;
        canvas.height = h;

        //画图
        ctx.drawImage(image, x, y, w, h, 0, 0, w, h);

        //返回img元素
        let img = document.createElement('img'); //等价于 new Image();
        img.src = canvas.toDataURL('image/png');
        return img;

    }

    //修改尺寸(支持长宽锁定)
    resize(image, width, height) {
        // console.log(image);
        if (!width) {
            width = image.width;
        }

        if (!height) {
            height = parseInt((width * image.height) / image.width); //高度等比例缩放
        }

        //创建画布
        let canvas = document.createElement('canvas');
        let ctx = canvas.getContext('2d');
        canvas.width  = width;
        canvas.height = height;

        //画图
        ctx.drawImage(image, 0, 0, width, height);

        //返回img元素
        let img = document.createElement('img'); //等价于 new Image();
        img.src = canvas.toDataURL('image/png');
        return img;
    }

    /** 
     * 合并图片, 横向
     * list_id 包含img元素的父元素 
     * gap: 间隔
     **/
    merge_image_row (list_id, gap=0) {
        let image_list = document.getElementById(list_id).getElementsByTagName('img');
        let total_width = 0;
        let max_height = 0;
        for (let i=0; i<image_list.length; i++) {
            total_width += image_list[i].width + gap;
            if (image_list[i].height > max_height) {
                max_height = image_list[i].height;
            }
        }

        //创建画布
        let canvas = document.createElement('canvas');
        let ctx = canvas.getContext('2d');
        canvas.width  = total_width;
        canvas.height = max_height;

        //画图
        let width_used = 0;
        for (let i=0; i<image_list.length; i++) {
            let image = image_list[i];
            ctx.drawImage(image, width_used, 0, image.width, image.height);
            width_used += image.width + gap;
        }
        

        //返回img元素
        let img = document.createElement('img'); //等价于 new Image();
        img.src = canvas.toDataURL('image/png');
        return img;

    }

    /** 
     * 合并图片, 横向
     * list_id 包含img元素的父元素 
     * gap: 间隔
     **/
    merge_image_col (list_id, gap=0) {
        let image_list = document.getElementById(list_id).getElementsByTagName('img');
        let total_height = 0;
        let max_width = 0;
        for (let i=0; i<image_list.length; i++) {
            total_height += image_list[i].height + gap;
            if (image_list[i].width > max_width) {
                max_width = image_list[i].width;
            }
        }

        //创建画布
        let canvas = document.createElement('canvas');
        let ctx = canvas.getContext('2d');
        canvas.width  = max_width;
        canvas.height = total_height;

        //画图
        let height_used = 0;
        for (let i=0; i<image_list.length; i++) {
            let image = image_list[i];
            ctx.drawImage(image, 0, height_used, image.width, image.height);
            height_used += image.height + gap;
        }
        

        //返回img元素
        let img = document.createElement('img'); //等价于 new Image();
        img.src = canvas.toDataURL('image/png');
        return img;

    }


    //显示覆层
    showMask (objImg) {
        let css = '#mask_image_box {position: fixed;top: 0;left: 0;bottom: 0;right: 0;padding-top: 16px;overflow: hidden;background-color: rgba(0,0,0,0.3);display:none;z-index: 200;}'
            +'#mask_image_box #mask_image {width:340px;position: absolute;top: 50%;left: 50%;transform: translate(-50%,-50%);}'
        let maskCss = document.getElementById('mask_image_box');
        if (!maskCss) {
            let style = document.createElement('style');
            style.innerText = css;

            let img = document.createElement('img');
            img.setAttribute('id', 'mask_image');

            let div = document.createElement('div');
            div.setAttribute('id', 'mask_image_box');
            div.setAttribute('onclick', "this.style.display='none'; return false;");

            div.appendChild(img);

            let body = document.getElementsByTagName('body')[0]
            body.appendChild(style);
            body.appendChild(div);

        }

        let imgSrc = objImg.getAttribute('src');
        document.getElementById('mask_image').setAttribute('src', imgSrc);
        document.getElementById('mask_image_box').style.display = 'block';
    }
}
