export default class zbImageQr {
    constructor() {
        this.canvas = null;
    }
    

    setCanvas (canvas) {
        this.canvas = canvas;
    }

    createQRCanvas (QRText, targetId) {
        //生成商品二维码
        $('#'+targetId).qrcode({text: QRText, quiet: 2});
        let QRCanvas = document.getElementById(targetId).getElementsByTagName('canvas')[0];

    };

    /**
     * 将以base64的图片url数据转换为Blob
     * @param urlData 用url方式表示的base64图片数据
     */
    convertBase64UrlToBlob(urlData){
        let arr = urlData.split(','), mime = arr[0].match(/:(.*?);/)[1],
            bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
        while(n--){
            u8arr[n] = bstr.charCodeAt(n);
        }
        return new Blob([u8arr], {type:mime});
    }

    download_canvas (canvas) {
        let a = document.createElement('a');
        a.download = 'download.png';
        a.href = canvas.toDataURL('image/png');
        a.click();
    }

    /**
     * 创建建二维码图片
     * https://kazuhikoarase.github.io/qrcode-generator/js/demo/
     * https://www.cnblogs.com/gygang/p/9116140.html
     * @param qrText 文字
     * @param parentId 父级元素ID
     * @param w 二维码宽度
     */
    createQR (qrText, parentId, w=200) {
        $('#'+parentId).qrcode({
            render:'canvas',
            size: w,
            quiet:2,
            text:qrText,
            });
    }

    //二维码图片下边加文字
    addTextToQR (qrParent, text, targetId) {
        //二维码
        let canvasQR = document.getElementById(qrParent).getElementsByTagName('canvas')[0];

        //创建画布
        let canvasElement = document.createElement('canvas');
        canvasElement.width = canvasQR.width;
        canvasElement.height = canvasQR.height + 20;
        

        //将二维码画到canvas中
        let ctx = canvasElement.getContext('2d');
        ctx.fillStyle = '#fff';
        ctx.fillRect(0, 0, canvasElement.width, canvasElement.height); //整个画布设置为白色(默认为透明)
        ctx.drawImage(canvasQR, 0, 0, canvasQR.width, canvasQR.height); //画上二维码

        //在下边写上文字
        ctx.font='12px Sans-serif';
        ctx.fillStyle = '#333';
        ctx.fillText(text, 5, canvasQR.height+10, canvasQR.width);

        //写入页面
        document.getElementById(targetId).appendChild(canvasElement);
        

        //将图片加入到指定的容器中
        // let img = document.createElement('img');
        // img.src = canvasElement.toDataURL('image/png');
        // img.setAttribute('class', imageClass);
        // let box = document.getElementById(parentId);
        // box.innerHTML = '';
        // box.appendChild(img);
    }

    //显示覆层
    showMask (objImg) {
        let css = '#mask_image_box {position: fixed;top: 0;left: 0;bottom: 0;right: 0;padding-top: 16px;overflow: hidden;background-color: rgba(0,0,0,0.3);display:none;z-index: 200;}'
            +'#mask_image_box #mask_image {width:340px;position: absolute;top: 50%;left: 50%;transform: translate(-50%,-50%);}'
        let maskCss = document.getElementById('mask_image_box');
        if (!maskCss) {
            let style = document.createElement('style');
            style.innerText = css;

            let img = document.createElement('img');
            img.setAttribute('id', 'mask_image');

            let div = document.createElement('div');
            div.setAttribute('id', 'mask_image_box');
            div.setAttribute('onclick', "this.style.display='none'; return false;");

            div.appendChild(img);

            let body = document.getElementsByTagName('body')[0]
            body.appendChild(style);
            body.appendChild(div);

        }

        let imgSrc = objImg.getAttribute('src');
        document.getElementById('mask_image').setAttribute('src', imgSrc);
        document.getElementById('mask_image_box').style.display = 'block';
    }
}
