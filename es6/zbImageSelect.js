//选择图片并回显到指定dom中(图片样式需要自己配置)
export default class zbImageSelect {
    constructor (config={}) {
        this.onSelected = config.onSelected ??= null; //选则图片后执行回调
        this.onClickImg = config.onClickImg ??= null; //点击图片缩略图后的回调

        this.files = {}; //存放自动生成的input对象, 可用于上传到后端
        this.params = config.params ??= null; //透传参数
    }
    

    //选择图片
    choseImage (config) {
        //初始化input标签
        let input = document.createElement('input');
        for (let k in config['attr']) {
            input.setAttribute(k, config['attr'][k]);
        }
        input.setAttribute('type', 'file');
        input.setAttribute('data-box-id', config.img_box_id);
        input.setAttribute('data-title', config.title ? config.title : '');
        input.addEventListener('input', this.changeImage.bind(this));

        let id = config['attr']['id'];
        this.files[id] = input;

        input.click(); //触发点击事件, 让用户选择图片
    }

    //改变输入的图片的事件
    changeImage (e) {
        let input = e.target;
        let file = input.files[0];
        if (!file.size) {
            alert('未找到图片, 请重新选择');
            return false;
        }

        //回显图片到页面指定位置
        let input_id = input.getAttribute('id');
        let image_box_id = input.getAttribute('data-box-id');
        let image_title = input.getAttribute('data-title');
        let imageBox = document.getElementById(image_box_id);
        let img_id = 'img_'+input_id;

        //如果已经存在, 就先移除
        let old = document.getElementById(img_id);
        if (old) {
            imageBox.removeChild(old);
        }

        let that = this;
        let reader = new FileReader();
        reader.onload = function(evt) {
            let img = document.createElement('img');
            img.setAttribute('id', img_id);
            img.setAttribute('title', image_title);
            if (typeof that.onClickImg === 'function') {
                img.addEventListener('click', that.onClickImg); //注册点击事件
            }
            img.src = evt.target.result;
            imageBox.appendChild(img);
        };

        reader.readAsDataURL(file);

        //选择图片后回调
        if (typeof this.onSelected === 'function') {
            this.onSelected({img_name: file.name, input_id: input_id}, this.params);
        }
    }
    
    //显示遮罩层预览
    showMask (objImg) {
        let css = '#mask_image_box {position: fixed;top: 0;left: 0;bottom: 0;right: 0;padding-top: 16px;overflow: hidden;background-color: rgba(0,0,0,0.3);display:none;z-index: 200;}'
            +'#mask_image_box #mask_image {width:340px;position: absolute;top: 50%;left: 50%;transform: translate(-50%,-50%);}';
        let maskCss = document.getElementById('mask_image_box');
        if (!maskCss) {
            let style = document.createElement('style');
            style.innerText = css;

            let img = document.createElement('img');
            img.setAttribute('id', 'mask_image');

            let div = document.createElement('div');
            div.setAttribute('id', 'mask_image_box');
            div.setAttribute('onclick', "this.style.display='none'; return false;");

            div.appendChild(img);

            let body = document.getElementsByTagName('body')[0]
            body.appendChild(style);
            body.appendChild(div);

        }

        let imgSrc = objImg.getAttribute('src');
        document.getElementById('mask_image').setAttribute('src', imgSrc);
        document.getElementById('mask_image_box').style.display = 'block';
    }

    download_canvas (canvas) {
        let a = document.createElement('a');
        a.download = 'download.png';
        a.href = canvas.toDataURL('image/png');
        a.click();
    }

    encodeObj (obj) {
        return encodeURIComponent(JSON.stringify(obj));
    }

    decodeObj (str) {
        return JSON.parse(decodeURIComponent(str));
    }
}
/**
 * 用法举例:
document.getElementById('choseImage').addEventListener('click', function(e){
    let si = new zbImageSelect({
        onSelected: function(args, params) {
            console.log(args);
            console.log(params);
            console.log(si.files);
        },
        onClickImg: function(e) {
            console.log(e.target);
            si.showMask(e.target);
        },
        params: {a:1, b:2}
    })

    si.choseImage({
        attr:{}, 
        img_box_id:'image_box', 
        tile:'image desc'
    })
});
 */
