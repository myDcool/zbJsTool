export default class zbList {
    constructor(config) {
        this.data = config.data ??= [];
        this.list_type = config.list_type ??= 'click';
        this.list_id = config.list_id ??= '';
        this.onClick = config.onClick ??= null;

        this.content = ''; //html字符串
        this.contentNode = null; //html DOM 节点

        //支持点击事件, 每一行最右边有一个箭头
        this.tplClick =
            '<div class="zb-list" id="{list_id}">\
                <div class="item" id="{item_id}" list_id="{list_id}">\
                    <div class="title">{title}</div>\
                    <div class="body">\
                        <div class="desc">{desc}</div>\
                        <div class="action zb-icon-right-arrow">&nbsp;</div>\
                    </div>\
                </div>\
            </div>';

        //不支持点击事件, 仅展示文字
        this.tplText =
            '<div class="zb-list" id="{list_id}">\
                <div class="item" id="{item_id}" list_id="{list_id}">\
                    <div class="title">{title}</div>\
                    <div class="body">\
                        <div class="desc">{desc}</div>\
                    </div>\
                </div>\
            </div>';

        this.init();
    }
    
    initCss() {
        let flag = document.getElementsByClassName('zb-list');
        if (!flag || flag.length === 0) {
            let style = document.createElement('style');
            style.innerText =
                '.hide{display:none}'+
                '.show{display:block}'+
                '.zb-icon-right-arrow {background-image: url(\'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEgAAABICAYAAABV7bNHAAAABmJLR0QA/wD/AP+gvaeTAAABOUlEQVR4nO3bP0oDQRxH8aciGEkTzBlsPYStWNjoPa29QIyggqVHSC+IfwoNKdx8+TXj7ur7wDRbzAyP2YVlWZAkSZI67DWc+xC4AI6BZ+C94VqjMwEegY/vsQCOet3RwFyyibMe94ww0m6jed86rp0AN4wwUgsHwJKfp2i0J6mFGXDL9kjz/rY2HEYqMFKBkQqMVGCkAiMVGKnASAVGKpiRX0uMhJFKjFRgpAIjFRipwEgFRiowUoGRCuZ8xeiKtAD2+9vacKRIZy0WbPVdTD3wFgt8SAfGCYwTGCcwTmCcwDiBcQLjBMYJjBMYJzBOYJzAOIFxAuMExgmMExgnmAB3bI/z739mueKPxGn1XazrV88H4BRYNVpzVKbAE5uTs2RkJ2dtp+HcU+AceAWugZeGa0mSJEm/6xOpQ0qay1FCYAAAAABJRU5ErkJggg==\'); background-size: 100% 100%; background-repeat: no-repeat;}' +
                '.zb-list {}' +
                '.zb-list .item {display: flex; flex-direction: row; justify-content: space-between; align-items: center; border-bottom: 1px solid #c8c8c8; margin: 4px; padding:2px;}' +
                '.zb-list .item .title {font-size: 14px; min-width:100px;}' +
                '.zb-list .item .body {display: flex; flex-direction: row; justify-content: flex-end; align-items: center}' +
                '.zb-list .item .desc {font-size: 14px; color: #888888;}' +
                '.zb-list .item .action {width: 18px;}' +
                ''
            ;

            let head = document.getElementsByTagName('head')[0];
            head.appendChild(style);
        }
    }

    getTpl (type) {
        let obj = {
            click: this.tplClick,
            text: this.tplText
        }

        return obj[type] ??= obj.click;
    }

    //初始化
    init(){
        this.initCss();
        let data = this.data;
        let list_type = this.list_type;
        let list_id = this.list_id;

        let node = '';
        let list_data = '';
        let tpl = this.getTpl(list_type);
        node = this.htmlToNode(tpl);
        list_data = [{list_id: list_id, item: data}];
        this.content = this.repeatNode(node, list_data);
        this.contentNode = this.htmlToNode(this.content);
        this.contentNode.addEventListener('click', this.click.bind(this)); //只监听item的父级(委托模式)
    }
    
    click(e){
        console.log(e);
        let ele = e.target;
        for (let i=0; i<10; i++) {
            if(ele.classList.contains('item')) {
                break;
            } else {
                //console.log();
                ele = ele.parentElement;
            }
        }
        let id = ele.getAttribute('id');
        let item = {};
        for (let i=0; i<this.data.length; i++) {
            if (this.data[i].item_id === id) {
                item = this.data[i];
                break;
            }
        }
        this.onClick(item, e.target);
    }

    //更新描述
    updateDesc (id, str='') {
        document.getElementById(id).getElementsByClassName('desc')[0].innerText = str;
    }

    //追加item到列表中
    append(data) {
        let list = document.getElementById(this.list_id);

        //获取item模板
        let tpl = this.getTpl(this.list_type);
        let node = this.htmlToNode(tpl);
        let str = node.getElementsByClassName('item')[0].outerHTML;

        //挨个组装, 并添加
        for (let i=0; i<data.length; i++) {
            data[i].list_id = this.list_id;
            let tmp_str = this.repeatString(str, [data[i]]);
            let tmp_node = this.htmlToNode(tmp_str);
            list.append(tmp_node);
        }
    }

    //覆盖指定id的dom元素
    replaceNode (id){
        let old = document.getElementById(id);
        let parent = old.parentNode;
        parent.replaceChild(this.contentNode, old);
    }

    //更新指定id的内容
    updateNode (id) {
        let contentBox = document.getElementById(id);
        contentBox.innerHTML = '<div id="tmp_list"></div>';
        let tmp = document.getElementById('tmp_list');
        contentBox.replaceChild(this.contentNode, tmp);
    }

    //覆盖指定id的html标签
    replaceHtml (id) {
        let dom = document.getElementById(id);
        if (dom) {
            dom.outerHTML = this.content;
        }
    }

    //将json编码, 可以作为html标签的属性
    encodeObj (obj) {
        return encodeURIComponent(JSON.stringify(obj));
    }

    //解码
    decodeObj (str) {
        return JSON.parse(decodeURIComponent(str));
    }

    htmlToNode(html) {
        let div = document.createElement('div');
        div.innerHTML = html;
        return div.firstElementChild;
    }

    repeatString (tplDom, arr, func=null) {
        if (tplDom.length === 0) {
            this.error('字符串长度为空');
            return;
        }

        if (arr.length === 0) {
            this.error('数据为空');
            return tplDom;
        }

        let tpl = tplDom;
        let out = '';
        for (let i=0; i<arr.length; i++) {
            if (typeof func === 'function') {
                arr[i] = func(arr[i]);
            }
            let map = arr[i];
            let tmp = tpl;
            for (let j in map) {
                let re = new RegExp('{' + j + '}', 'g');
                tmp = tmp.replace(re, map[j]);
            }

            let re = new RegExp('{_idx}', 'g');
            tmp = tmp.replace(re, parseInt(i)+1);

            out += tmp;
        }

        return out;
    };

    /**
     * @param node HTML DOM节点, 注意不是string
     * @param arr json数组 注意是数组类型
     * @return string 返回HTML字符串, 注意不是DOM节点
     */
    repeatNode (node, arr) {
        let out = [];
        for (let i=0; i<arr.length; i++) {
            let tmp = node.outerHTML;
            tmp = tmp.replace(/\s/g, ' '); //去掉回车换行, 减少空白符

            let map = arr[i];

            //先渲染内层的数组
            for (let j in map) {
                if (map[j] instanceof Array) { //数组, 递归替换
                    let subNode = node.querySelector('.'+j);
                    if (subNode) {
                        let subHtml = this.repeatNode(subNode, map[j]); //递归
                        let subTpl = subNode.outerHTML.replace(/\s/g, ' ');
                        tmp = tmp.replace(subTpl, subHtml);
                    }
                }
            }

            //再渲染内层的对象
            for (let j in map) {
                if (map[j] instanceof Object && !(map[j] instanceof Array)) { //对象, 递归替换
                    let subNode = node.querySelector('.'+j);
                    if (subNode) {
                        let subHtml = this.repeatNode(subNode, [map[j]]); //递归
                        let subTpl = subNode.outerHTML.replace(/\s/g, ' ')
                        tmp = tmp.replace(subTpl, subHtml);
                    }
                }
            }

            //最后渲染外层的键值对/字符串
            for (let j in map) {
                if (typeof map[j] === 'string' || typeof map[j] === 'number') { //字符串, 直接替换
                    let re = new RegExp('{' + j + '}', 'g');
                    tmp = tmp.replace(re, map[j]);
                }
            }

            out.push(tmp);
        }

        return out.join('');
    }

    error (str){
        console.log('zbList Error: '+str);
    }

}

/**
 * 用法举例:
let mylist = new zbList({
    list_type: 'click', //click|text
    list_id: 'target',
    data: [
        {
            item_id: '1111',
            title:'测试-name1',
            desc: 'Hello World!',
        },
        {
            item_id: '2222',
            title:'测试-name2',
            desc: 'Hello World!',
        },
    ],
    onClick: function(item_data, clicked){
        console.log(item_data); //列表项元素
        console.log(clicked); //被直接点击的元素
    }
});
mylist.replaceNode('target'); ////将id=content的节点替换为list



let children = [
    {
       item_id: '3333',
       title:'测试-name3',
       desc: 'Hello World!',
    },
    {
       item_id: '4444',
       title:'测试-name4',
       desc: '<p>Hello World!</p>',
    },
];
mylist.append(children);
 */
