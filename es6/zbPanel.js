export default class {
    constructor(config) {
        this.data = config.data ??= [];
        this.panel_id = config.panel_id ??= '';
        this.onHide = config.onHide ??= null; //隐藏body后回调
        this.onShow = config.onShow ??= null; //显示body后回调
        this.content = '';
        this.contentNode = null;
        this.tpl =
            '<div class="zb-panel-list">\
                <div class="box" id="{panel_id}">\
                    <div class="title" id="pt_{title_id}" data-title-id="{title_id}">\
                        <div class="title-idx">{idx}</div>\
                        <div class="title-name">{name}</div>\
                        <div class="title-icon zb-icon-down-arrow">&nbsp;</div>\
                    </div>\
                    <div class="body hide" id="pb_{body_id}"> {content} </div>\
                    <div class="footer hide" id="pf_{footer_id}"> {content} </div>\
                </div>\
            </div>';

        //初始化css
        this.initCss();

        //data需要是数组, 表示有多个panel
        let d = this.data;
        for (let i=0; i<d.length; i++) {
            let pid = d[i]['panel_id'];
            d[i]['title']['title_id'] = pid;
            d[i]['title']['idx'] = d[i]['title']['idx'] ? d[i]['title']['idx'] : '';
            d[i]['body']['body_id'] = pid;
            d[i]['footer']['footer_id'] = pid;
        }

        let node = this.htmlToNode(this.tpl);
        this.content = this.repeatNode(node, [{box:d}]);
        this.contentNode = this.htmlToNode(this.content);

        let items = this.contentNode.getElementsByClassName('title');
        for (let i=0; i<items.length; i++) {
            let title_id = items[i].getAttribute('data-title-id');
            items[i].addEventListener('click', this.togglePanel.bind(this, title_id));
        }
    }
    
    initCss() {
        let flag = document.getElementById('zbPanelCss');
        if (!flag) {
            let style = document.createElement('style');
            style.setAttribute('id', 'zbPanelCss');
            style.innerText =
                '.hide{display:none}'+
                '.show{display:block}'+
                '.zb-icon-down-arrow {background-image: url(\'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEgAAABICAYAAABV7bNHAAAABmJLR0QA/wD/AP+gvaeTAAABNElEQVR4nO3WTU7CQBxA8UcIuuASnkV3Lohn8WOtJ9CjCAsPodEzKLdQQ1IXziQNWtrSaUM675fMhsX8py8dACRJkiRJkiRJkiRJkkZoDpwAkwFmTcKs+QCzOpsCD8AGKIAP4LTHeWfAOszaAPfhDAfrht/DltcXsOhh1iLsvT3vqodZybzx98AF8A1cJJxzDnxWzHpJOCe5V/4/dMpIu+IUwHOCGb25pvrgKa5b1bUqr8sO+/duCizpJ1KTOEsO/EsaYAY8svtB2l63umtVAE/AcZInGEDKSKOLE6WINNo4UZdIo48T7RMpmzhRm0jZxYmOgBX1fwHqfspXYa9RavImZffmbNs3UhZxoraRsooTNY2UZZyoLlLWcaKqSMYpmQF3wHtYt+EzSZIkSZIkSZIkNfIDdOkSxgq5DmAAAAAASUVORK5CYII=\'); background-size: 100% 100%; background-repeat: no-repeat;}' +
                '.zb-icon-up-arrow {background-image: url(\'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEgAAABICAYAAABV7bNHAAAABmJLR0QA/wD/AP+gvaeTAAABIUlEQVR4nO3YTWrCQBxA8VdEC16id3HnRnqWfhygF/Aq6qZ3aGnPYI9R6UIXzmwK+TDOxDB5Pwi4CPMfHpkQBEmSJEmSJEmSJLU3Bd6AH2Affk9vuqMBmQEb4Pjvegfub7ivQaiKYySa44w6Uts4o4x0aZxRReoaZxSRZsCO+gCHcNXdswtrFaXNk3MAHoEl8Ntwb1FP0iVxotFE6hInKj7SNXGiYiOliBMVFyllnKiYSBNgS3OcVYe1VzR/AmzDHgbrlTxxojaRnq9YP7tv0h2rKk3H7TPBjGy+yBsnqov0kXBOci+kP1ZVqo7bU4ZZyUyANfDHebN7YJFx3oLz37THMHPNwF/S0Rx46GnWXZg172meJEmSJEmSJEmSJElSf04syRLHPtlBcwAAAABJRU5ErkJggg==\'); background-size: 100% 100%; background-repeat: no-repeat;}' +
                '.zb-panel-list {padding: 2px; display: flex; flex-direction: column; justify-content: flex-start; align-items: stretch;}' +
                '.zb-panel-list .box {border-bottom: 1px solid #c8c8c8;}' +
                '.zb-panel-list .title {display: flex;flex-direction: row;justify-content: space-between; align-items: center; padding: 5px;}' +
                '.zb-panel-list .title-idx {max-width: 50px;overflow-x: scroll; padding: 3px 0;}' +
                '.zb-panel-list .title-idx::-webkit-scrollbar {display: none;}' +
                '.zb-panel-list .title-name {flex-grow: 1;overflow-x: scroll; padding: 3px 0; margin-left: 8px;}' +
                '.zb-panel-list .title-name::-webkit-scrollbar {display: none;}' +
                '.zb-panel-list .title-icon {width: 18px;}' +
                '.zb-panel-list .body {padding: 5px;margin-top: 3px;}' +
                '.zb-panel-list .footer {padding: 5px; font-size:12px; text-align:right;}'
            ;

            let head = document.getElementsByTagName('head')[0];
            head.appendChild(style);
        }
    }

    //用本插件生成的node, 替换指定id的节点
    replaceDom (id) {
        let old = document.getElementById(id);
        let parent = old.parentNode;
        parent.replaceChild(this.contentNode, old);
    }

    //用本插件生成的node, 替换指定id的节点
    replaceNode (id) {
        let old = document.getElementById(id)
        let parent = old.parentNode;
        parent.replaceChild(this.contentNode, old);
    }

    //更改node
    updateNode (id) {
        let contentBox = document.getElementById(id);
        contentBox.innerHTML = '<div id="tmp_nav_content"></div>';
        let tmp = document.getElementById('tmp_nav_content');
        contentBox.replaceChild(this.contentNode, tmp);
    }

    //追加panel到列表中
    append (data) {
        let list = document.getElementById(this.panel_id);

        //data需要是数组, 表示有多个panel
        for (let i=0; i<data.length; i++) {
            let panel_id = data[i]['panel_id'];
            data[i]['title']['title_id'] = panel_id;
            data[i]['title']['idx'] = data[i]['title']['idx'] ? data[i]['title']['idx'] : '';
            data[i]['body']['body_id'] = panel_id;
            data[i]['footer']['footer_id'] = panel_id;
        }

        let content = this.repeatNode(this.htmlToNode(this.tpl), [{box:data}]);
        let boxes = this.htmlToNode(content).children;
        while (boxes.length) {
            list.appendChild(boxes[0]);
        }

        //每次循环boxes的元素会减少？？
        // const len = boxes.length;
        // for (let i = 0; i < len; i++) {
        // 	list.appendChild(boxes[i]);
        // }
    }

    //展开/折叠body/footer
    togglePanel (panel_id) {
        let box = document.getElementById(panel_id);
        let title_action = box.getElementsByClassName('title')[0].getElementsByClassName('title-icon')[0];

        let hide = 'zb-icon-up-arrow';
        let show = 'zb-icon-down-arrow';

        let display = '';
        if (title_action.classList.contains(hide)) { //当前是上箭头
            display = 'hide';
            title_action.classList.remove(hide);
            title_action.classList.add(show);
            if (typeof this.onHide === 'function') {
                this.onHide(panel_id);
            }
        } else {
            display = 'show';
            title_action.classList.remove(show);
            title_action.classList.add(hide);
            if (typeof this.onShow === 'function') {
                this.onShow(panel_id);
            }
        }

        let body = document.getElementById('pb_'+panel_id);
        body.classList.remove('hide');
        body.classList.remove('show');
        body.classList.add(display);

        let footer = document.getElementById('pf_'+panel_id);
        footer.classList.remove('hide');
        footer.classList.remove('show');
        footer.classList.add(display);
    }

    //更新某个panel的body内容
    updateBodyContent(panel_id, content) {
        document.getElementById('pb_'+panel_id).innerHTML = content;
    }

    updateBodyNode (panel_id, node) {
        let body = document.getElementById('pb_'+panel_id);
        body.innerHTML = '';
        body.appendChild(node);
    }

    //更新某个panel的footer内容
    updateFooterContent(panel_id, content) {
        document.getElementById('pf_'+panel_id).innerHTML = content;
    }

    getTitleId(id) {
        return 'pt_'+id;
    }

    getBodyId(id) {
        return 'pb_'+id;
    }

    getFooterId(id) {
        return 'pt=f_'+id;
    }

    htmlToNode(html) {
        let div = document.createElement('div');
        div.innerHTML = html;
        return div.firstElementChild;
    }

    repeatString (tplDom, arr, func=null) {
        if (tplDom.length === 0) {
            this.error('字符串长度为空');
            return;
        }

        if (arr.length === 0) {
            this.error('数据为空');
            return tplDom;
        }

        let tpl = tplDom;
        let out = '';
        for (let i=0; i<arr.length; i++) {
            if (typeof func === 'function') {
                arr[i] = func(arr[i]);
            }
            let map = arr[i];
            let tmp = tpl;
            for (let j in map) {
                let re = new RegExp('{' + j + '}', 'g');
                tmp = tmp.replace(re, map[j]);
            }

            let re = new RegExp('{_idx}', 'g');
            tmp = tmp.replace(re, parseInt(i)+1);

            out += tmp;
        }

        return out;
    };

    /**
     * @param node HTML DOM节点, 注意不是string
     * @param arr json数组 注意是数组类型
     * @return string 返回HTML字符串, 注意不是DOM节点
     */
    repeatNode (node, arr) {
        let out = [];
        for (let i=0; i<arr.length; i++) {
            let tmp = node.outerHTML;
            tmp = tmp.replace(/\s/g, ' '); //去掉回车换行, 减少空白符

            let map = arr[i];

            //先渲染内层的数组
            for (let j in map) {
                if (map[j] instanceof Array) { //数组, 递归替换
                    let subNode = node.querySelector('.'+j);
                    if (subNode) {
                        let subHtml = this.repeatNode(subNode, map[j]); //递归
                        let subTpl = subNode.outerHTML.replace(/\s/g, ' ');
                        tmp = tmp.replace(subTpl, subHtml);
                    }
                }
            }

            //再渲染内层的对象
            for (let j in map) {
                if (map[j] instanceof Object && !(map[j] instanceof Array)) { //对象, 递归替换
                    let subNode = node.querySelector('.'+j);
                    if (subNode) {
                        let subHtml = this.repeatNode(subNode, [map[j]]); //递归
                        let subTpl = subNode.outerHTML.replace(/\s/g, ' ');
                        tmp = tmp.replace(subTpl, subHtml);
                    }
                }
            }

            //最后渲染外层的键值对/字符串
            for (let j in map) {
                if (typeof map[j] === 'string' || typeof map[j] === 'number') { //字符串, 直接替换
                    let re = new RegExp('{' + j + '}', 'g');
                    tmp = tmp.replace(re, map[j]);
                }
            }

            out.push(tmp);
        }

        return out.join('');
    }

}

/**
 * 样例
let panel = new zbPanel({
    panel_id: 'target',
    data: [
        {
           'panel_id': '1111',
           'title':{
               'idx':1, //可以不写
               'name':'测试-name1',
           },
           'body':{ 'content': '<p>Hello World!</p>' },
           'footer':{ 'content':'1111111' }
       },
        {
           'panel_id': '222',
           'title':{
               'idx':2,
               'name':'测试-name2',
           },
           'body':{ 'content': '<p>Hello World!</p>' },
           'footer':{ 'content': '222222' }
       }
    ],
    onHide: function(id){},
    onShow: function(id){panel.updateBodyContent(id, '内容更新啦~' + id);}
});

panel.replaceNode('target'); //将id为target的元素替换成本插件生成的node
 */
