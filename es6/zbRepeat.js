export default class zbRepeat {
    constructor(){}
    
    htmlToNode(html) {
        let div = document.createElement('div');
        div.innerHTML = html;
        return div.firstElementChild;
    }

    /**
     * @param node HTML DOM节点, 注意不是string
     * @param arr json数组 注意是数组类型
     * @return string 返回HTML字符串, 注意不是DOM节点
     */
    repeatNode (node, arr) {
        console.log(node.outerHTML);
        let out = [];
        for (let i=0; i<arr.length; i++) {
            let tmp = node.outerHTML;
            tmp = tmp.replace(/\s/g, ' '); //去掉回车换行, 减少空白符
            let map = arr[i];

            //先渲染内层的数组
            for (let j in map) {
                if (map[j] instanceof Array) { //数组, 递归替换
                    let subNode = node.querySelector('.'+j);
                    if (subNode) {
                        let subHtml = this.repeatNode(subNode, map[j]); //递归
                        let subTpl = subNode.outerHTML.replace(/\s/g, ' ');
                        tmp = tmp.replace(subTpl, subHtml);
                    }
                }
            }

            //再渲染内层的对象
            for (let j in map) {
                if (map[j] instanceof Object && !(map[j] instanceof Array)) { //对象, 递归替换
                    let subNode = node.querySelector('.'+j);
                    if (subNode) {
                        let subHtml = this.repeatNode(subNode, [map[j]]); //递归
                        let subTpl = subNode.outerHTML.replace(/\s/g, ' ')
                        tmp = tmp.replace(subTpl, subHtml);
                    }
                }
            }

            //最后渲染外层的键值对/字符串
            for (let j in map) {
                if (typeof map[j] === 'string' || typeof map[j] === 'number') { //字符串, 直接替换
                    let re = new RegExp('{' + j + '}', 'g');
                    tmp = tmp.replace(re, map[j]);
                }
            }

            out.push(tmp);
        }

        return out.join('');
    }

    //替换字符串
    repeatString (tplDom, arr, func=null) {
        if (tplDom.length === 0) {
            console.log('字符串长度为空');
            return;
        }

        if (arr.length === 0) {
            console.log('数据为空');
            return tplDom;
        }

        let tpl = tplDom;
        let out = '';
        for (let i=0; i<arr.length; i++) {
            if (typeof func === 'function') {
                arr[i] = func(arr[i]);
            }
            let map = arr[i];
            let tmp = tpl;
            for (let j in map) {
                let re = new RegExp('{' + j + '}', 'g');
                tmp = tmp.replace(re, map[j]);
            }

            let re = new RegExp('{_idx}', 'g');
            tmp = tmp.replace(re, parseInt(i)+1);

            out += tmp;
        }

        return out;
    };

    //替换dom内层html内容
    repeatInnerHTML (selector, arr, func=null) {
        let tplDom = document.querySelector(selector);
        if (!tplDom) {
            console.log('未找到: '+selector);
            return;
        }

        tplDom.innerHTML = this.repeatString(tplDom.innerHTML, arr, func);
    }

    //替换dom
    repeatOuterHTML (selector, arr, func=null) {
        let tplDom = document.querySelector(selector);
        if (!tplDom) {
            console.log('未找到: '+selector);
            return;
        }

        tplDom.outerHTML = this.repeatString(tplDom.outerHTML, arr, func);
    };
}

/**
    //展示效果
    let repeat = new zbRepeat();
    let json_list = [{name:'张三', age:18, fav:1},{name:'李四', age:18, fav:2},{name:'王五', age:18, fav:3},{name:'赵六', age:18, fav:4}];
    
    //循环innerHTML
    repeat.repeatInnerHTML('#tpl_repeat_inner', json_list); 

    //循环outerHTML
    function maping(obj) {
        let fav = '';
        switch (obj.fav) {
            case 1: fav = '篮球'; break;
            case 2: fav = '足球'; break;
            case 3: fav = '地球'; break;
            default: fav = '语文';break;
        }
        obj.fav = fav;
        return obj;
    }
    repeat.repeatOuterHTML('.tpl_repeat_outer', json_list, maping); 
    
    //循环嵌套HTML
    let json_nest_list = [{"aa":"aa","bb":"bb","mm":"1","cc":[{"n":"n1","v":"v1","mm":"2"},{"n":"n2","v":"v2","mm":"2"},{"n":"n3","v":"v3","mm":"2"}],"dd":{"d1":"d1","d2":"d2","d3":[{"x":"x1","y":"y1","mm":"4"},{"x":"x2","y":"y2","mm":"4"},{"x":"x3","y":"y3","mm":"4"}],"mm":"3"}}];
    let node = document.getElementById('tpl_repeat_node');
    let html = repeat.repeatNode(node, json_nest_list);
    document.getElementById('repeat_node').innerHTML = html;
    
 */
