export default class zbRouter {
    constructor(routers=[], defaultRule='') {
        this.domain = location.origin + '/';
        this.urlPath = location.pathname;
        this.routers = routers;
        this.defaultRule = defaultRule;

        this.splitChar1 = ',';
        this.splitChar2 = ':';
        this.catchChar = '@'; //正则表达式捕获的值,存放在以此字符开头的变量中
        this.matchedParams = {}; //当前匹配到的路由的参数

        this.pages = {}; //页面元素, 防止重复调用 document.getElementById
        this.page_class = 'page'; //页面的class
        this.page_class_show = 'show'; //显示页面的class
        this.page_class_hide = 'hide'; //隐藏页面的class

        this.init();
    }

    //初始化
    init() {
        let that = this;
        window.onpopstate = function(event) {
            //点击浏览器的回退按钮, 和调用 history.pushState()方法时都会触发此事件
            console.log('onpopstate');
            console.log(event);
            that.route(location.pathname, '2');
            //return false;
        };
    };
    
    //匹配到路由, 执行回调前执行的逻辑, 一般是切换页面; 可参考自行修改
    hookBefore(args){
        //hook, 执行回调前执行
        console.log(args);
        //将class='page'的元素都隐藏掉, 然后显示id=args['target']
        if (args.hasOwnProperty('tpl') && args.hasOwnProperty('target')) {
            //将模板内容放到目标元素内
            if (this.pages.hasOwnProperty(args.target) === false) {
                this.pages[args.target] = document.getElementById(args.target);
            }
            
            if (this.pages.hasOwnProperty(args.tpl) === false) {
                this.pages[args.tpl] = document.getElementById(args.tpl);
            }
            
            this.pages[args.target].innerHTML = this.pages[args.tpl].innerHTML;
            
            //隐藏其他page
            let eles = document.getElementsByClassName(this.page_class);
            for (let i=0; i<eles.length; i++) {
                eles[i].classList.remove(this.page_class_show);
                eles[i].classList.add(this.page_class_hide);
            }

            //显示目标元素
            let target = document.getElementById(args.target);
            target.classList.remove(this.page_class_hide);
            target.classList.add(this.page_class_show);
        }
    }; 
    
    //执行路由
    route (args, replace='0') {
        let str = this.encodeRule(args);
        if (str === location.search.substring(1)) {
            replace = 'do_nothing';
            console.log('当前页面跟路由一样');
        }

        let rule = '';
        if (args.hasOwnProperty('m') && this.routers.hasOwnProperty(args.m)) {
            rule = args.m;
        } else {
            rule = this.defaultRule;
        }        
        
        //更换浏览器地址栏URL
        let curUrl = this.buildUrl(str);
        if (replace === '0') {
            console.log('pushState: '+curUrl);
            history.pushState({url:curUrl}, "", curUrl);
            this.urlPath = str;
        } else if (replace === '1') {
            console.log('replaceState: '+curUrl);
            history.replaceState({url:curUrl}, "", curUrl);
            this.urlPath = str;
        } else {
            // do noting
        }

        let router = this.routers[rule];
        
        //调用回调前hook
        if (this.hookBefore && (typeof this.hookBefore === 'function')) {
            this.hookBefore(router, args);
        }
        
        //调用路由中的回调
        if (router.callback && (typeof router.callback === 'function')) {
            router.callback(args);
        }

    };

    //更改浏览器地址栏URL，并追加入history
    pushUrl (uri) {
        let url = this.buildUrl(uri);
        history.pushState(null, "", url);
        this.urlPath = url;
    }

    //更改浏览器地址栏URL，并替换history当前地址
    replaceUrl (uri) {
        let url = this.buildUrl(uri);
        history.replaceState(null, "", url);
        this.urlPath = url;
    }

    //将字符串拆分成键值对
    explodeArgs (str) {
        let arr = str.split(this.splitChar1);
        let list = {};
        for (let i = 0; i < arr.length; i++) {
            let tmp = arr[i].split(this.splitChar2);
            list[tmp[0]] = tmp[1];
        }

        return list;
    }

    //公共error处理方法
    error (str) {
        console.log(str);
    }

    //解析查询参数成对象
    decodeRule (rule='') {
        if (rule.length === 0) {
            rule = location.search.substring(1);
        }
        console.log(rule);

        rule = decodeURIComponent(rule);
        if (rule.length === 0) {
            return {};
        }

        let items = null, item = null, name = null, value = null;

        if (rule.indexOf('&') === -1) {
            items = rule.split("=");
            name = items[0];
            value = items[1];
            let tmp = {};
            tmp[name] = value;
            return tmp;
        }

        let args = {};
        items = rule.split("&");
        for(let i=0; i < items.length; i++){
            item = items[i].split("=");
            if(item[0]){
                name = item[0];
                value = item[1] ? item[1] : "";
                args[name] = value;
            }
        }

        return args;
    };

    encodeRule (obj) {
        if (obj) {
            let arr = [];
            for (let k in obj) {
                arr.push(k +'=' + obj[k]);
            }
            return arr.join('&');
        } else {
            return '';
        }
    }

    //组装url
    buildUrl (obj=null) {
        let search = this.encodeRule(obj);
        return location.origin + location.pathname + '?' + search;
    };

    loading(id, txt='请稍后, 加载中...') {
        document.getElementById(id).innerHTML = '<div style="padding: 20px; text-align: center">'+txt+'</div>'
    }

}

/**
 * 示例
 * http://www.test7.com/test.html?m=1&a=1&b=3
//路由规则
let routers = {
    index: {tpl:'t1', target:'p1', callback: function(args){console.log(args);}},
    list: {tpl:'t2', target:'p2',callback: function(args){console.log(args);}},
    detail: {tpl:'t3', target:'p3',callback: function(args){console.log(args);}},
};
let r = new zbRouter(rule, 'index');
r.hookBefore = function(router, args){
    //hook, 执行回调前执行, 比如切换页面
    console.log(rule);
    console.log(args);
};

r.route(r.decodeRule()); //根据当前url地址执行路由
 */
