export default class zbTool {
    constructor(){}

    isArray (o){
        return Object.prototype.toString.call(o) === '[object Array]';
    }

    //查找元素在二维数组中的索引值
    arraySearch (arr, key, val) {
        for (let i=0; i<arr.length; i++) {
            if (arr[i][key] !== 'undefined' && arr[i][key] === val) {
                return arr[i];
            }
        }
        return false;
    }
    
    //获取数组中某一列的值
    arrayColumn(arr, key, index='') {
        if (index.length === 0) {
            let ls = [];
            for(let i=0; i<arr.length; i++) {
                ls.push(arr[i][key]);
            }
            return ls;
        } else {
            let obj = {};
            for(let i=0; i<arr.length; i++) {
                let idx = arr[i][index];
                let val = arr[i][key];
                obj[idx] = val;
            }
            return obj;
        }
    }

    //根据某一列分组
    arrayGroup(arr, key, val='') {
        let obj = {};
        if (val.length == 0) {
            for (let i=0;i<arr.length; i++) {
                let v = arr[i][key];
                if (!(v in obj)) {
                    obj[v] = [];
                }
                obj[v].push(arr[i]);
            }
        } else {
            for (let i=0;i<arr.length; i++) {
                let v = arr[i][key];
                if (!(v in obj)) {
                    obj[v] = [];
                }
                obj[v].push(arr[i][val]);
            }
        }
        

        return obj;
    }

    //用回调函数处理数组的每一个元素
    formatArray (arr, func) {
        for (let i=0; i<arr.length; i++) {
            arr[i] = func(arr[i]);
        }

        return arr;
    }

    //抽取部分字段, 并重命名
    pickArray(arr, fields) {
        let ls = [];
        for (let i=0; i<arr.length; i++) {
            let item = {};
            for (let k in fields) {
                let v = fields[k];
                item[k] = arr[i][v] ??= '';
            }
            ls.push(item);
        }
        return ls;
    }

    //获取随机数
    getRandom (len) {
        let char = '';
        for (let i=0; i<len; i++) {
            char += Math.floor(Math.random()*10);
        }

        return char;
    }

    getYear () {
        let myDate = new Date();
        return myDate.getFullYear();
    }

    getMonth () {
        let myDate = new Date();
        return myDate.getMonth() + 1;
    }

    getDay () {
        let myDate = new Date();
        return myDate.getDate();
    }

    //给字符串str补上前导字符pad, 最终使str总长为length
    strPad (str, pad, length) {
        let padn = length - str.length;
        let pads = '';

        for (i =0; i < padn ; i++) {
            pads += pad;
        }

        return pads + str;
    }

    getToday (gap='-') {
        let d = new Date();
        d.setTime(d.getTime());
        let year = d.getFullYear();
        let month = d.getMonth()+1;
        let day = d.getDate();
        month = this.strPad(month.toString(),'0', 2 );
        day = this.strPad(day.toString(), '0', 2);
        return year + gap + month + gap + day;
    }

    getNowTime() {
        // 创建一个新的Date对象
        let now = new Date();

        // 获取年份
        let year = now.getFullYear();

        // 获取月份（注意：月份是从0开始的，所以实际月份需要加1）
        let month = now.getMonth() + 1;
        this.strPad(month.toString(),'0', 2 )

        // 获取日期
        let day = now.getDate();
        this.strPad(day.toString(), '0', 2);

        // 获取小时
        let hours = now.getHours();
        this.strPad(hours.toString(), '0', 2);

        // 获取分钟
        let minutes = now.getMinutes();
        this.strPad(minutes.toString(), '0', 2);

        // 获取秒钟
        let seconds = now.getSeconds();
        this.strPad(seconds.toString(), '0', 2);

        // 输出结果
        return year + month + day + hours + minutes + seconds;
        //console.log(`${year}-${month}-${date} ${hours}:${minutes}:${seconds}`);
    }

    //根据时间戳获取日期
    getSomeday (timestamp, gap='-') {
        let d = new Date(timestamp);
        let year = d.getFullYear();
        let month = d.getMonth()+1;
        let day = d.getDate();
        month = this.strPad(month.toString(),'0', 2 );
        day = this.strPad(day.toString(), '0', 2);
        return year + gap + month + gap + day;
    }

    //将字符串拆分成键值对
    explodeArgs (str, char1, char2) {
        let arr = str.split(char1);
        let list = {};
        for (let i = 0; i < arr.length; i++) {
            let tmp = arr[i].split(char2);
            list[tmp[0]] = tmp[1];
        }

        return list;
    }

    buildUrl(prefix, obj) {
        let param = this.parseQuery(prefix);

        for (let i in obj) {
            param[i] = obj[i]; //去掉重复的键, 用obj中的覆盖prefix中的
        }

        let arr = [];
        for (let j in param) {
            arr.push(j + '=' + param[j]);
        }

        let str = arr.join('&');

        let url = '';
        if (prefix.indexOf('?') !== -1) {
            let u =prefix.split('?');
            url = u[0] + '?' + str;
        } else {
            url = prefix + '?' + str;
        }

        return url;
    }

    parseQuery(query) {
        if (!query) {
            query = location.href;
        }
        let pos = query.indexOf('?');
        if (pos === -1) {
            return {};
        }

        query = query.substring(pos + 1);
        query = decodeURIComponent(query);

        if (query.length === 0) {
            return {};
        }

        let items = null, item = null, name = null, value = null;

        if (query.indexOf('&') === -1) {
            items = query.split("=");
            name = items[0];
            value = items[1]
            let tmp = {};
            tmp[name] = value;
            return tmp;
        }

        let args = {};
        items = query.split("&");
        for(let i=0; i < items.length; i++){
            item = items[i].split("=");
            if(item[0]){
                name = item[0];
                value = item[1] ? item[1] : "";
                args[name] = value;
            }
        }

        return args;
    }

    go_home() {
        window.location.href = '/';
    }

    //高亮底边
    activeBorderBottom(id, color='#7E57C2', cls='active_border_bottom') {
        let obj = document.getElementById(id);
        if (obj) {
            let had = document.getElementsByClassName(cls);
            for (let i=0; i<had.length; i++) {
                had[i].style.borderBottom = '';
                had[i].classList.remove(cls);
            }
            
            if (cls === 'active_border_bottom') {
                obj.style.borderBottom = '2px solid '+color;
            }
            
            obj.classList.add(cls);
        }
    }

    //获取嵌套数组/对象中的值
    // let obj = {'aa': {'xx':'cc', '0': 0, '1': '1', '-1': -1}}; getObjChild(obj, 'aa.xx')
    getObjChild (obj, key, gap='.') {
        let tmp = {};
        let arr = key.split(gap); //将key拆成数组

        if (obj[arr[0]]) {
            tmp = obj[arr[0]];
            for (let i=1; i<arr.length; i++) { //逐层进入匹配查找
                let is_found = false;
                if (typeof tmp === 'object') {
                    for(let j in tmp) {
                        if (j === arr[i]) {
                            tmp = tmp[j];
                            is_found = true;
                            break;
                        }
                    }
                }

                if (!is_found) {
                    tmp = false;
                    console.log('未找到 '+ arr[i]);
                    break;
                }
            }
        } else {
            console.log('未找到 '+ arr[0]);
            tmp = false;
        }

        return tmp;
    }

    redirect (url, isNew=false){
        if (isNew) {
            window.open(url);
        } else {
            window.location.href = url;
        }
    }

    refresh (isForce = true) {
        window.location.reload(isForce);
    }

    reload (isForce = true) {
        window.location.reload(isForce);
    }

    stringToEntity(str,radix){
        let arr=str.split('');
        radix=radix||0;
        let tmp=arr.map(item=>
            `&#${(radix?'x'+item.charCodeAt(0).toString(16):item.charCodeAt(0))};`).join('')
        console.log(`'${str}' 转实体为 '${tmp}'`)
        return tmp;
    }

    entityToString(entity){
        let entities=entity.split(';');
        entities.pop();
        let tmp=entities.map(item=>String.fromCharCode(
            item[2]==='x'?parseInt(item.slice(3),16):parseInt(item.slice(2)))).join('')
        console.log(`'${entity}' 转字符串为 '${tmp}'`)
        return tmp;
    }

    encodeObj (obj) {
        return encodeURIComponent(JSON.stringify(obj));
    }

    decodeObj (str) {
        return JSON.parse(decodeURIComponent(str));
    }

    htmlToNode(html) {
        let div = document.createElement('div');
        div.innerHTML = html;
        return div.firstElementChild;
    }

    /**
     * @param node HTML DOM节点, 注意不是string
     * @param arr json数组 注意是数组类型
     * @return string 返回HTML字符串, 注意不是DOM节点
     */
    repeatNode (node, arr) {
        let out = [];
        for (let i=0; i<arr.length; i++) {
            let tmp = node.outerHTML;
            tmp = tmp.replace(/\s/g, ' '); //去掉回车换行, 减少空白符

            let map = arr[i];

            //先渲染内层的数组
            for (let j in map) {
                if (map[j] instanceof Array) { //数组, 递归替换
                    let subNode = node.querySelector('.'+j);
                    if (subNode) {
                        let subHtml = this.repeatNode(subNode, map[j]); //递归
                        let subTpl = subNode.outerHTML.replace(/\s/g, ' ');
                        tmp = tmp.replace(subTpl, subHtml);
                    }
                }
            }

            //再渲染内层的对象
            for (let j in map) {
                if (map[j] instanceof Object && !(map[j] instanceof Array)) { //对象, 递归替换
                    let subNode = node.querySelector('.'+j);
                    if (subNode) {
                        let subHtml = this.repeatNode(subNode, [map[j]]); //递归
                        let subTpl = subNode.outerHTML.replace(/\s/g, ' ')
                        tmp = tmp.replace(subTpl, subHtml);
                    }
                }
            }

            //最后渲染外层的键值对/字符串
            for (let j in map) {
                if (typeof map[j] === 'string' || typeof map[j] === 'number') { //字符串, 直接替换
                    let re = new RegExp('{' + j + '}', 'g');
                    tmp = tmp.replace(re, map[j]);
                }
            }

            out.push(tmp);
        }

        return out.join('');
    }

    //替换字符串
    repeatString (tplDom, arr, func=null) {
        if (tplDom.length === 0) {
            console.log('字符串长度为空');
            return;
        }

        if (arr.length === 0) {
            console.log('数据为空');
            return tplDom;
        }

        let tpl = tplDom;
        let out = '';
        for (let i=0; i<arr.length; i++) {
            if (typeof func === 'function') {
                arr[i] = func(arr[i]);
            }
            let map = arr[i];
            let tmp = tpl;
            for (let j in map) {
                let re = new RegExp('{' + j + '}', 'g');
                tmp = tmp.replace(re, map[j]);
            }

            let re = new RegExp('{_idx}', 'g');
            tmp = tmp.replace(re, parseInt(i)+1);

            out += tmp;
        }

        return out;
    };

    //替换dom内层html内容
    repeatInnerHTML (selector, arr, func=null) {
        let tplDom = document.querySelector(selector);
        if (!tplDom) {
            console.log('未找到: '+selector);
            return;
        }

        tplDom.innerHTML = this.repeatString(tplDom.innerHTML, arr, func);
    }

    //替换dom
    repeatOuterHTML (selector, arr, func=null) {
        let tplDom = document.querySelector(selector);
        if (!tplDom) {
            console.log('未找到: '+selector);
            return;
        }

        tplDom.outerHTML = this.repeatString(tplDom.outerHTML, arr, func);
    };

    getHtmlById (id){
        return document.getElementById(id).innerHTML;
    };
    
    setHtmlById (id, str=''){
        document.getElementById(id).innerHTML = str;
    };

    replaceInnerHtmlById(srcId, targetId){
        let str = document.getElementById(srcId).innerHTML;
        document.getElementById(targetId).innerHTML = str;
    }

    //替换dom节点，id: 被替换的dom的id，str: 替换后的内容
    replaceDom (id, str){
        let dom = document.getElementById(id);
        if (dom) {
            dom.outerHTML = str;
        }
    }
    
    appendChild(id, node) {
        let dom = document.getElementById(id);
        dom.appendChild(node);
    }

    getInputs (id, tagName) {
        let data = {};
        let arrTagName = tagName.split(',');
        for (let j=0; j<arrTagName.length; j++) {
            let tname = arrTagName[j];
            let inputs = document.getElementById(id).getElementsByTagName(tname);
            for (let i=0; i<inputs.length; i++) {
                let k = inputs[i].name;
                let v = inputs[i].value;

                let type = inputs[i].type;
                if (type === 'checkbox') {
                    if (inputs[i].checked === true) {
                        v = 1;
                    } else {
                        v = 0;
                    }
                }

                data[k] = v;
            }
        }

        console.log(data);
        return data;
    }

    goScrollTop () {
        //把内容滚动指定的像素数（第一个参数是向右滚动的像素数，第二个参数是向下滚动的像素数）
        //向上是负数，向下是正数
        window.scrollBy(0, -100);
        //延时递归调用，模拟滚动向上效果
        scrolldelay = setTimeout('goScrollTop()', 30);
        //获取scrollTop值，声明了DTD的标准网页取document.documentElement.scrollTop，
        //否则取document.body.scrollTop；因为二者只有一个会生效，另一个就恒为0，所以取和值可以得到网页的真正的scrollTop值
        let sTop = document.documentElement.scrollTop + document.body.scrollTop;
        //判断当页面到达顶部，取消延时代码（否则页面滚动到顶部会无法再向下正常浏览页面）
        if (sTop <= 0) {
            clearTimeout(scrolldelay);
        }
    }

    //获取地理位置, 但使用的是 googleapi
    getPosition () {
        if ('geolocation' in navigator) {
            navigator.geolocation.getCurrentPosition(function(pos){
                console.log(pos)
            }, function(err){
                console.log(err)
            })

        }
    }

    //获取枚举值的名字
    getListNameByValue (obj, val) {
        for (let k in obj) {
            if (k === val) {
                return obj[k];
            }
        }

        return val;
    }

    htmlToNode (html) {
        let div = document.createElement('div');
        div.innerHTML = html;
        return div.firstElementChild;
    }

    //显示某个元素, 并隐藏有相同class的元素
    showElement(id, cls) {
        let eles = document.getElementsByClassName(cls);
        for (let i=0; i<eles.length; i++) {
            eles[i].classList.remove('show');
            eles[i].classList.add('hide');
        }

        document.getElementById(id).classList.remove('hide');
        document.getElementById(id).classList.add('show');
    }
    
    getById(id) {
        return document.getElementById(id);
    }
    
    getByClass (cls) {
        return document.getElementsByClassName(cls);
    }
    
    getByTag (tag) {
        return document.getElementsByTagName(tag);
    }
    
    //获取某个id下边的指定class
    getByIdClass(id, cls) {
        let node = document.getElementById(id);
        if (node) {
            return node.getElementsByClassName(cls);
        } else {
            return [];
        }
    }
    
    //获得嵌套的最内层id
    getByNestId(arr) {
        let node = null;
        for (let i=0; i<arr.length; i++) {
            let id = arr[i];
            node = document.getElementById(id);
            if (!node) {
                return null;
            }
        }
        return node;
    } 
    
    //对象中是否存在某个键
    isset(obj, key) {
        return obj.hasOwnProperty(key);
    }
    
    //合并两个对象
    mergeObj(obj1, obj2) {
        let obj = {};
        for (let i in obj1) {
            if (obj1.hasOwnProperty(i)) {
                obj[i] = obj1[i];
            }
        }
        
        for (let i in obj2) {
            if (obj2.hasOwnProperty(i)) {
                obj[i] = obj2[i];
            }
        }
        
        return obj;
    }
    
    hide(id){
        document.getElementById(id).classList.remove('hide');
        document.getElementById(id).classList.add('hide');
    }
    
    show(id){
        document.getElementById(id).classList.remove('hide');
    }
    
    localStorageSet(key, val) {
        return localStorage.setItem(key, val);
    }
    
    localStorageGet(key) {
        return localStorage.getItem(key);
    }
    
    getParentNode(ele, cls, max=100) {
        for (let i=0; i<max; i++) {
            if(ele.classList.contains(cls)) {
                return ele;
            } else {
                ele = ele.parentElement;
            }
        }
    }
    
    //逗号隔开的字符串值, 转为内容
    idxToName (mapping, idxs, gap=',') {
        let arr = [];
        if (typeof(idxs) === 'string') {
            arr = idxs.split(gap);
        } else {
            arr = idxs;
        }
        
        let names = [];
        for (let i=0; i<arr.length; i++) {
            if (mapping[arr[i]]) {
                names.push(mapping[arr[i]]);
            }
        }
        return names;
    }
    
    //追加行到表格中
    appendTr(parent, arr, tag='td') {
        if (typeof parent === 'string') {
            parent = this.getById(parent);
        }
        
        let tr = document.createElement('tr');
        
        for (let i=0; i<arr.length; i++) {
            let cell = document.createElement(tag);
            cell.innerHTML = arr[i];
            tr.appendChild(cell);
        }
        
        parent.appendChild(tr);
    }

    //切换显示 box: 父级div  item: 要显示的内部div
    switchContent(box, item) {
        let items = document.getElementById(box).children;
        for (let i=0; i<items.length; i++) {
            if (!items[i].classList.contains('hide')) {
                items[i].classList.add('hide');
            }

            if (items[i].getAttribute('id') == item) {
                items[i].classList.remove('hide');
            }
        }
    }

    //修改地址栏的URL
    changeUrl(url, obj) {
        let arr = [];
        for (let k in obj) {
            arr.push(k +'=' +obj[k]);
        }
        if (arr.length > 0) {
            if (url.indexOf('?') !== -1) {
                url += '&' + arr.join('&');
            } else {
                url += '?' + arr.join('&');
            }
        }

        history.pushState({url:url}, '', url);

        return url;
    }
}
