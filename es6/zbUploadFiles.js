export default class zbUploadImage {
    constructor(config={}) {
        this.fileInputId = config.file_input_id ??= 'file'; //input标签id
        this.file = config.file ??= null; //input节点对象
        this.uploadUrl = config.upload_url ??= '';
        this.params = config.params ??= {};
        this.uploadSuccess = config.uploadSuccess ??= null;
        this.uploadFailed = config.uploadFailed ??= null;
    }

    upOne () {
        let file;
        if (this.file) {
            file = this.file;
        } else {
            file = document.getElementById(this.fileInputId).files[0];
        }

        let form = new FormData(); // FormData 对象
        form.append(this.fileInputId, file);
        for(let k in this.params) {
            form.append(k, this.params[k]);
        }

        this.up(form);
    }

    upLot () {
        let form = new FormData(); // FormData 对象
        form.append('params', this.params);

        let files = document.getElementById(this.fileInputId).files;
        for (let i=0; i<files.length; i++) {
            form.append(`file_${i}`, files[i]);
        }

        this.up(form);
    }

    up (form) {
        fetch(this.uploadUrl, {
            method: "POST",
            body: form
        })
        .then((response) => response.json())
        .then((rs) => {
            this.uploadSuccess(rs);
        })
        .catch((error) => {
            this.uploadFailed(error);
        })
    }
}

/**
 * 用法举例
document.getElementById('file').addEventListener('input', function() {
    let imageUp = new zbUploadImage({
        upload_url: 'http://www.test7.com/test.php',
        file_input_id: 'file',
        params: {a:1, b:2},
        uploadSuccess: function (rs) {
            console.log(rs);
        },
        uploadFailed: function (error) {
            console.log(error);
        }
    });
    imageUp.upOne(); //上传一个
    //imageUp.upLot(); //上传多个
})
 */
