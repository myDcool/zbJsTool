function parseQuery(query) {
    let pos = query.indexOf('?');
    if (pos === -1) {
        return {};
    }

    query = query.substring(pos + 1);
    query = decodeURIComponent(query);

    if (query.length === 0) {
        return {};
    }

    let items = null, item = null, name = null, value = null;

    if (query.indexOf('&') === -1) {
        items = query.split("=");
        name = items[0];
        value = items[1]
        let tmp = {};
        tmp[name] = value;
        return tmp;
    }

    let args = {};
    items = query.split("&");
    for(let i=0; i < items.length; i++){
        item = items[i].split("=");
        if(item[0]){
            name = item[0];
            value = item[1] ? item[1] : "";
            args[name] = value;
        }
    }

    return args;
}

function buildUrl(prefix, obj) {
    let param = parseQuery(prefix);

    for (let i in obj) {
        param[i] = obj[i]; //去掉重复的键, 用obj中的覆盖prefix中的
    }

    let arr = [];
    for (let j in param) {
        arr.push(j + '=' + param[j]);
    }

    let str = arr.join('&');

    let url = '';
    if (prefix.indexOf('?') !== -1) {
        let u =prefix.split('?');
        url = u[0] + '?' + str;
    } else {
        url = prefix + '?' + str;
    }

    return url;
}

function isSuccess(data) {
    if (data.code && data.code === 1) {
        return true;
    } else {
        return false;
    }
}

function go_home() {
    window.location.href = '/';
}

function getPathInfo(spliteChar='_') {
    return location.pathname.substring(1).split(spliteChar);
}

function getToday() {
    let d = new Date();
    d.setTime(d.getTime());
    let year = d.getFullYear();
    let month = d.getMonth()+1;
    let day = d.getDate();
    month = strPad(month.toString(),'0', 2 );
    day = strPad(day.toString(), '0', 2);
    return year +"-" + month + "-" + day;
}

//给字符串str补上前导字符pad, 最终使str总长为length
function strPad(str, pad, length) {
    var padn = length - str.length;
    var pads = '';

    for (i =0; i < padn ; i++) {
        pads += pad;
    }

    return pads + str;
}

function goScrollTop() {
    //把内容滚动指定的像素数（第一个参数是向右滚动的像素数，第二个参数是向下滚动的像素数）
    //向上是负数，向下是正数
    window.scrollBy(0, -100);
    //延时递归调用，模拟滚动向上效果
    scrolldelay = setTimeout('goScrollTop()', 30);
    //获取scrollTop值，声明了DTD的标准网页取document.documentElement.scrollTop，
    //否则取document.body.scrollTop；因为二者只有一个会生效，另一个就恒为0，所以取和值可以得到网页的真正的scrollTop值
    var sTop = document.documentElement.scrollTop + document.body.scrollTop;
    //判断当页面到达顶部，取消延时代码（否则页面滚动到顶部会无法再向下正常浏览页面）
    if (sTop <= 0) {
        clearTimeout(scrolldelay);
    }
}

function base64UrlToBlob(urlData) {
    let arr = urlData.split(','), mime = arr[0].match(/:(.*?);/)[1],
        bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
    while(n--){
        u8arr[n] = bstr.charCodeAt(n);
    }
    return new Blob([u8arr], {type:mime});
}

function togglePanel(obj, show='+', hide='-') {

    let display = '';
    if (obj.innerText === show) {
        display = 'block';
        obj.innerText = hide;
    } else {
        display = 'none';
        obj.innerText = show;
    }

    let childs = obj.parentNode.parentNode.children;
    for (let i=1; i<childs.length; i++) {
        childs[i].style.display = display;
    }
}
