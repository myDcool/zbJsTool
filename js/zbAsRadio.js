//单选列表
const zbAsRadio = function(){
    this.onSelected = null; //点击确认后执行回调
    this.isConfirm = 0; //是否已点击过确认按钮, 防止频繁点击
    this.onShow = null;//显示弹窗前的回调
    this.onHide = null;//点击空白关闭弹窗时的回调
    this.data = null; //选项
    this.params = {};//透传参数
    
    //浮层模板
    this.tpl = '<div id="zbAsRadio" class="zb-flex-col-bottom zb-flex-nowrap zb-width-r100">\
        <div id="zbRadioMask" class="zb-flex-grow1"></div>\
        <div id="zbRadioBox" class="zb-flex-col-between zb-flex-stretch">\
          <div id="zbRadioTitle" class="zb-text-center"></div>\
          <div id="zbRadioBody" class="zb-scroll-y"></div>\
         </div>\
         <input type="hidden" id="zbRadioInput" value="">\
        </div>';

    this.init = function (title='', data=null, callback=null) {
        let flag = document.getElementById('zbAsRadio');
        if (flag === null) {
            //初始化css
            this.initCss();

            //初始模板, 绑定事件
            let body = document.getElementsByTagName('body')[0];
            let node = this.htmlToNode(this.tpl);
            node.querySelector('#zbRadioMask').addEventListener('click', this.hide.bind(this));
            body.appendChild(node);
        }

        //标题
        document.getElementById('zbRadioTitle').innerText = title;
        if (data) {
            this.addList(data);
        }
        if (typeof callback === 'function') {
            this.onSelected = callback;
        }

        return this;
    }

    this.htmlToNode = function (html) {
        let div = document.createElement('div');
        div.innerHTML = html;
        return div.firstElementChild;
    }

    //隐藏弹出层
    this.hide = function () {
        document.getElementById('zbAsRadio').remove(); //彻底移除了心静
        if (typeof this.onHide === 'function') {
            this.onHide();
        }
    }

    //显示弹出层
    this.show = function () {
        if (typeof this.onShow === 'function') {
            this.onShow();
        }
        document.getElementById('zbAsRadio').style.height = '100%';
    }

    //点击确认按钮触发执行
    this.confirm = function () {
        if (this.isConfirm === 1) {
            console.log('重复点击');
            return;
        }
        this.isConfirm = 1;
        let input = document.getElementById('zbRadioInput');
        let obj = this.decodeObj(input.value);
        obj.params = this.params;
        input.value = ''; //清空已存储的数据
        this.hide();
        if (typeof this.onConfirm === 'function') {
            this.onConfirm(obj);
            this.params = {};
        }
    }

    //生成样式
    this.initCss = function() {
        let id = 'zbAsRadioCss';
        if (document.getElementById(id)) {
            return;
        }
        let style = document.createElement('style');
        style.setAttribute('id', id);
        style.innerText =
            '.zb-flex-col-top {display:flex;flex-direction:column;justify-content:flex-start}'+
            '.zb-flex-col-bottom {display:flex;flex-direction:column;justify-content:flex-end}'+
            '.zb-flex-col-between {display:flex;flex-direction:column;justify-content:space-between}'+
            '.zb-flex-col-center {display:flex;flex-direction:column;justify-content:center}'+
            '.zb-flex-col-around {display:flex;flex-direction:column;justify-content:space-around}'+
            '.zb-flex-row-left {display:flex;flex-direction:row;justify-content:start}'+
            '.zb-flex-row-center {display:flex;flex-direction:row;justify-content:center}'+
            '.zb-flex-row-around {display:flex;flex-direction:row;justify-content:space-around}'+
            '.zb-flex-row-between {display:flex;flex-direction:row;justify-content:space-between}'+
            '.zb-flex-row-right {display:flex;flex-direction:row;justify-content:flex-end}'+
            '.zb-flex-stretch {align-items:stretch}'+
            '.zb-flex-center {align-items:center}'+
            '.zb-flex-grow1 {flex-grow:1}'+
            '.zb-flex-nowrap {flex-wrap:nowrap}'+
            '.zb-text-center {text-align:center}'+
            '.zb-scroll-x {overflow-x:scroll; white-space: nowrap;}'+
            '.zb-scroll-x::-webkit-scrollbar {display:none}'+
            '.zb-scroll-y {overflow-y:scroll; white-space: nowrap;}'+
            '.zb-scroll-y::-webkit-scrollbar {display:none}'+
            '.zb-scroll{overflow:scroll; white-space: nowrap;}'+
            '.zb-scroll::-webkit-scrollbar {display:none}'+
            '.zb-width-r100{width:100%;}'+
            '.zb-height-r100{height:100%;}'+
            '#zbAsRadio{position:fixed;top:0;height:0;z-index:200;overflow:hidden;background-color:rgba(0,0,0,0.4);}'+
            '#zbAsRadio #zbRadioMask{}'+
            '#zbAsRadio #zbRadioBox{max-height:90%;background-color:#fff;border-top-left-radius:6px;border-top-right-radius:6px;}'+
            '#zbAsRadio #zbRadioTitle{height:40px;line-height:40px;font-size:20px;border-bottom:1px solid #eeeeee;}'+
            '#zbAsRadio #zbRadioBody{min-height:100px;}'+
            '#zbAsRadio .item_center{height:30px;margin:5px;text-align:center;border-bottom:1px solid #e8e8e8}'+
            '#zbAsRadio .item_left{height:30px;margin:5px;border-bottom:1px solid #e8e8e8}'+
            '#zbAsRadio .item_img{height:30px;width:30px;margin:5px;}'+
            '@keyframes bg_color {from{background:#eee;} to{background:#fff;}}'+
            '@-webkit-keyframes bg_color {from{background:#eee;} to{background:#fff;}}'
        ;

        let head = document.getElementsByTagName('head')[0];
        head.appendChild(style);
    }

    //覆盖指定id的dom元素
    this.replaceNode = function (id, node){
        let old = document.getElementById(id);
        let parent = old.parentNode;
        parent.replaceChild(node, old);
    }

    /**
     * 添加纵向列表数据
     * @param array data [{title:'aaa', value:'aaa', img:'', a:1, b2}] 或 [1,2,3]
     * @param object config 
     */
    this.addList = function(data, config={}) {
        this.data = data;
        let tplListItem = '<div class="zbas-list-item {item_class}" data-params="{params}" data-index="{idx}">{title}</div>';
        let tplListItemImg = '<div class="zbas-list-item zb-flex-row-left zb-flex-center {item_class}" data-params="{params}" data-index="{idx}"><img class="{item_img_class}" src="{img}">{title}</div>';

        let dataBody = document.getElementById('zbRadioBody');
        let strItems = '';
        for (let i=0; i<data.length; i++) {
            let item = data[i];
            if (this.isObject(item)) {
                // [{title:'aaa', value:'aaa', img:'', a:1, b2}]
                let tpl = (item.img !== undefined) ? tplListItemImg : tplListItem;
                strItems += this.repeatString(tpl, [item], function(row){
                    row.item_class = config.item_class ? config.item_class : 'item_center';
                    row.item_img_class = config.item_img_class ? config.item_img_class : 'item_img';
                    row.idx = i;
                    return row;
                });
            } else {
                //[1,2,3]
                strItems += this.repeatString(tplListItem, [item], function(v){
                    let row = {title: v, value: v, idx:i};
                    row.item_class = config.item_class ? config.item_class : 'item_center';
                    return row;
                });
            }
            
        }

        //渲染列表
        dataBody.innerHTML = strItems;

        //添加事件绑定
        let items = dataBody.getElementsByClassName('zbas-list-item');
        for (let i=0; i<items.length; i++) {
            items[i].addEventListener('click', this.click.bind(this));
        }

        return this;
    }

    //记录点击的list值
    this.click = function (e) {
        let obj = e.target;        
        if (this.isConfirm === 1) {
            return;
        } else {
            this.isConfirm = 1;
        }

        if (typeof this.onSelected === 'function') {
            let idx = obj.getAttribute('data-index');
            let d = this.data[idx];
            d.args = this.params;
            this.hide();
            this.onSelected(d);
        }
        
        return true;
    }

    //根据数组, 渲染HTML字符串
    this.repeatString = function (tplDom, arr, func=null) {
        if (!tplDom.length) {
            this.error('字符串长度为空');
            return;
        }

        if (arr.length === 0) {
            this.error('数据为空');
            return tplDom;
        }

        let tpl = tplDom;
        let out = '';
        for (let i=0; i<arr.length; i++) {
            if (typeof func === 'function') {
                arr[i] = func(arr[i]);
            }
            let map = arr[i];
            let tmp = tpl;
            for (let j in map) {
                let re = new RegExp('{' + j + '}', 'g');
                tmp = tmp.replace(re, map[j]);
            }

            let re = new RegExp('{_idx}', 'g');
            tmp = tmp.replace(re, parseInt(i)+1);

            out += tmp;
        }

        return out;
    };

    //是否是数组
    this.isArray = function (o){
        return Object.prototype.toString.call(o) === '[object Array]';
    }

    this.isObject = function (o) {
        return Object.prototype.toString.call(o) === '[object Object]';
    }

    this.error = function(str) {
        console.log(str);
    }

    this.encodeObj = function (obj) {
        return encodeURIComponent(JSON.stringify(obj));
    }

    this.decodeObj = function (str) {
        return JSON.parse(decodeURIComponent(str));
    }
}
/**
 * 用法举例:
    let radio = new zbAsRadio();
    radio.params = {a:1, b:2}; //透传参数

    // let data = [1,2,3,4,5,6,7,8,9,0];
    // let data = [{title:'a', value:1},{title:'b', value:2},{title:'c', value:3},{title:'d', value:4},{title:'e', value:5}];
    let data = [
        {title:'a', value:1, img:'summer/icons/right-arrow.png'},
        {title:'b', value:2, img:'summer/icons/right-arrow.png'},
        {title:'c', value:3, img:'summer/icons/right-arrow.png'},
        {title:'d', value:4, img:'summer/icons/right-arrow.png'},
        {title:'e', value:5}
    ];
    
    radio.init('this is title');
    radio.onSelected = function(params) {
        console.log(params); //里边有透传的参数
    }
    radio.onShow = function(){};
    radio.onHide = function(){};
    radio.addList(data, {item_class:'item_left', item_img_class:'item_img'}); //可选 item_center,item_left,item_img
    radio.show();
 */
