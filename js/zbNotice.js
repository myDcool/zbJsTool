const zbNotice = function(){

    this.text = function (str, timeout=3000, bgColor='rgba(100, 100, 100, 0.8)') {
        let noticeList = document.getElementById('zbNoticeList');
        if (!noticeList) {
            noticeList = document.createElement('div');
            noticeList.setAttribute('id', 'zbNoticeList');

            let style = document.createElement('style');
            style.innerText =
                '#zbNoticeList {position: fixed; top: 0; left: 0; z-index: 600; width: 100%; display: flex; flex-direction: column; justify-content: center; align-items: center;} '+
                '.zbNoticeItem {width:80%; margin: 10px 0 0 0; padding: 10px; white-space: nowrap; text-overflow: ellipsis; overflow: hidden; word-break: break-all; border-radius:5px; color:#f8f8f8; background-color: '+ bgColor +'}';

            let body = document.getElementsByTagName('body')[0];

            body.appendChild(style);
            body.appendChild(noticeList);

            setInterval(function(){
                let now = Date.now();
                let items = noticeList.getElementsByClassName('zbNoticeItem');
                for (let i = 0; i < items.length; i++) {
                    let time = parseFloat(items[i].getAttribute('data-time'));
                    if (now >= time) {
                        noticeList.removeChild(items[i]);
                    }
                }

            }, 1000);
        }

        let notice = document.createElement('div');
        notice.classList.add('zbNoticeItem');
        notice.setAttribute('data-time', Date.now() + timeout);
        notice.innerText = str;

        noticeList.appendChild(notice);

    }
}
// let nt = new zbNotice();
// nt.text('xxxx');