const zbUploadImage = function () {
    this.xhr = null;
    this.fileInputId = 'file';
    this.uploadUrl = '';
    this.uploadSuccess = null;
    this.uploadFailed = null;
    this.blobImage =null;
    this.extension = '.png';

    /**
     * 将以base64的图片url数据转换为Blob
     * @param urlData 用url方式表示的base64图片数据
     */
    this.convertBase64UrlToBlob = function(urlData){
        let arr = urlData.split(','), mime = arr[0].match(/:(.*?);/)[1],
            bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
        while(n--){
            u8arr[n] = bstr.charCodeAt(n);
        }
        return new Blob([u8arr], {type:mime});
    }

    //上传文件方法
    this.UploadImage = function(params={}, async=true) {
        let form = new FormData(); // FormData 对象
        let xhr = new XMLHttpRequest();  // XMLHttpRequest 对象
        console.log('图片上传地址: '+this.uploadUrl);
        let that = this;
        xhr.open("post", this.uploadUrl, async); //post方式，url为服务器请求地址，true 该参数规定请求是异步处理。
        xhr.onload = function (evt) {
            //console.log(evt); //打印完整响应
            if (typeof that.uploadSuccess === 'function') {
                that.uploadSuccess(xhr.response);
            }
        }; //请求完成
        xhr.onerror = function (evt) {
            if (typeof that.uploadFailed === 'function') {
                that.uploadFailed(evt.target.responseText);
            }
        }; //请求失败

        form.append(this.fileInputId, this.blobImage, "image_"+Date.parse(new Date())+this.extension); // 文件对象
        for (let i in params) {
            form.append(i, params[i]);
        }
        xhr.send(form); //开始上传，发送form数据
    }
}

/**
 * 用法举例
    let imageTool = new zbUploadImage();
    imageTool.uploadUrl = 'http://xxx';
    imageTool.fileInputId = 'input_id';
    imageTool.uploadSuccess = function (evt) {
        //服务断接收完文件返回的结果
        let rs = JSON.parse(evt.target.responseText);
        if (rs.code === 1) {
            notice.innerText = '上传成功';
            let img = document.getElementById(img_id);
            img.setAttribute('src', rs.data.image); //更新图片地址
        } else {
            console.log(rs.msg);
        }
    };
    imageTool.uploadFailed = function (evt) {
        notice.innerText = "上传失败！" + evt.target.responseText;
    }
    imageTool.blobImage = imageTool.convertBase64UrlToBlob(rst.base64);
    imageTool.UploadImage({goods_id: goods_id, img_id: img_id});
 */
